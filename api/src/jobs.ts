import { getRepository, LessThan } from "typeorm";
import consola from "consola";
import dayjs from "dayjs";
import { Log } from "./constants";
import { User } from "./entities";

const deleteUser = async (user: User) =>
    await getRepository(User)
        .delete(user)
        .catch((error) => consola.error(error));

const deleteUsers = async (users: User[]) => {
    const result = users.map((user) => deleteUser(user));
    return result.length > 0
        ? consola.info(Log.CRON_JOB, "Expired users are deleted")
        : consola.info(Log.CRON_JOB, "No users to delete");
};

export const expireUsers = async (): Promise<void> => {
    // eslint-disable-next-line
    const aDayAgo = dayjs().subtract(1, "day").toISOString();

    return await getRepository(User)
        .find({
            where: { createdAt: LessThan(aDayAgo), confirmed: false },
        })
        .then((list) => deleteUsers(list))
        .catch((error) => consola.error(Log.CRON_JOB_ERROR, error));
};

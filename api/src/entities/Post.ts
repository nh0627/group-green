import {
    Entity,
    Column,
    ManyToOne,
    ManyToMany,
    JoinColumn,
    JoinTable,
    OneToMany,
} from "typeorm";
import * as validator from "class-validator";
import Default from "./Default";
import { User, Hashtag, Comment } from "./";

@Entity("posts")
class Post extends Default {
    @Column()
    @validator.IsString()
    url!: string;

    @Column({ nullable: true })
    @validator.IsString()
    @validator.IsOptional()
    description?: string;

    @Column({ nullable: true })
    @validator.IsString()
    @validator.IsOptional()
    location?: string;

    @Column()
    @validator.IsDate()
    modifiedAt: Date = new Date();

    @ManyToOne(() => User, (user) => user.posts)
    @JoinColumn()
    user!: User;

    @OneToMany(() => Comment, (comment) => comment.post)
    comments?: Comment[];

    @ManyToMany(() => Hashtag)
    @JoinTable()
    hashtags?: Hashtag[] | string[];

    @ManyToMany(() => User, (user) => user.liked, { cascade: true })
    @JoinTable()
    likes!: User[];
}

export default Post;

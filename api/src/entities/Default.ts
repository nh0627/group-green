import { PrimaryGeneratedColumn, Column } from "typeorm";
import { IsDate } from "class-validator";

class Default {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    @IsDate()
    createdAt: Date = new Date();
}

export default Default;

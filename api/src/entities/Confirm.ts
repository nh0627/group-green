import { Entity, OneToOne, JoinColumn, Column } from "typeorm";
import { IsHash } from "class-validator";
import Default from "./Default";
import User from "./User";
import { HASH_FUNCTION } from "../constants";
const hashFunction = (process.env.HASH_FUNCTION as string) || HASH_FUNCTION;

@Entity("confirms")
class Confirm extends Default {
    @Column()
    @IsHash(hashFunction)
    key!: string;

    @OneToOne(() => User, (user) => user.id, { onDelete: "CASCADE" })
    @JoinColumn()
    user!: User;
}

export default Confirm;

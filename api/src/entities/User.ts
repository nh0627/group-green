import { Entity, Column, OneToMany, ManyToMany } from "typeorm";
import * as validator from "class-validator";
import Default from "./Default";
import { Post } from "./";
import { Comment } from "./";
import { IsNotDuplicated } from "./decorators";
import { HASH_FUNCTION } from "../constants";
const hashFunction = (process.env.HASH_FUNCTION as string) || HASH_FUNCTION;

@Entity("users")
class User extends Default {
    @Column({ unique: true })
    @validator.IsEmail()
    @validator.IsNotEmpty()
    @IsNotDuplicated()
    email!: string;

    @Column({ select: false })
    @validator.IsNotEmpty()
    @validator.IsHash(hashFunction)
    password?: string;

    @Column({ unique: true })
    @validator.IsNotEmpty()
    @validator.IsAlphanumeric()
    @validator.Length(1, 10)
    @IsNotDuplicated()
    username!: string;

    @Column()
    @validator.IsBoolean()
    confirmed: boolean = false;

    @Column()
    @validator.IsBoolean()
    active: boolean = false;

    @Column()
    @validator.IsDate()
    modifiedAt: Date = new Date();

    @Column({ nullable: true })
    @validator.IsString()
    @validator.IsOptional()
    nickname?: string;

    @Column({ nullable: true })
    @validator.IsString()
    @validator.IsOptional()
    description?: string;

    @Column({ nullable: true })
    @validator.IsUrl()
    @validator.IsOptional()
    imgUrl?: string;

    @OneToMany(() => Post, (post) => post.user)
    posts?: Post[];

    @OneToMany(() => Comment, (comment) => comment.user)
    comments?: Comment[];

    @ManyToMany(() => Post, (post) => post.likes)
    liked?: Post[];
}

export default User;

import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from "class-validator";
import { getRepository } from "typeorm";
import { ErrorMessage } from "../constants";

@ValidatorConstraint({ async: true })
class IsNotDuplicatedConstraint implements ValidatorConstraintInterface {
    validate(value: string | number, args: ValidationArguments) {
        const { object, property } = args;
        const entity = object.constructor;
        const condition: Record<string, unknown> = {};
        condition[property] = value;
        return getRepository(entity)
            .count(condition)
            .then((count: number) => (count === 0 ? true : false));
    }
}

export const IsNotDuplicated = (
    validationOptions?: ValidationOptions,
    // eslint-disable-next-line @typescript-eslint/ban-types
): ((object: object, propertyName: string) => void) => {
    const options = { ...validationOptions, message: ErrorMessage.DUPLICATION };
    // eslint-disable-next-line @typescript-eslint/ban-types
    return (object: object, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options,
            constraints: [],
            validator: IsNotDuplicatedConstraint,
        });
    };
};

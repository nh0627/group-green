import { Entity, Column } from "typeorm";
import * as validator from "class-validator";
import Default from "./Default";

@Entity("hashtags")
class HashTag extends Default {
    @Column()
    @validator.IsString()
    name!: string;
}

export default HashTag;

import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { User } from ".";
import Default from "./Default";
import Post from "./Post";
import * as validator from "class-validator";

@Entity("comments")
class Comment extends Default {
    @Column()
    @validator.IsString()
    text!: string;

    @Column()
    @validator.IsDate()
    modifiedAt: Date = new Date();

    @ManyToOne(() => User, (user) => user.comments)
    @JoinColumn()
    user!: User;

    @ManyToOne(() => Post, (post) => post.comments)
    @JoinColumn()
    post!: Post;
}

export default Comment;

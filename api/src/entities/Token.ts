import { Entity, OneToOne, JoinColumn, Column } from "typeorm";
import { IsJWT } from "class-validator";
import Default from "./Default";
import User from "./User";

@Entity("tokens")
class Token extends Default {
    @Column()
    @IsJWT()
    token!: string;

    @OneToOne(() => User, (user) => user.id, { onDelete: "CASCADE" })
    @JoinColumn()
    user!: User;
}

export default Token;

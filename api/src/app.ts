import "reflect-metadata";
import express, { Express } from "express";
import { createConnection } from "typeorm";
import cors from "cors";
import consola from "consola";
import passport from "passport";
import cron from "node-cron";
import { expireUsers } from "./jobs";
import { Log, Routes, PORT } from "./constants";
import * as routes from "./routes";
import {
    errorHandler,
    notFoundErrorHandler,
    setPassportStrategy,
} from "./middlewares";
import { AddressInfo } from "net";

const portNum = (process.env.PORT as string) || PORT;

const registerMiddlewares = (app: Express) => {
    const limit = "50mb";
    app.use(express.json({ limit }));
    app.use(express.urlencoded({ extended: true, limit }));
    app.use(cors());
    app.use(
        express.urlencoded({
            extended: true,
        }),
    );
    app.use(passport.initialize());
    setPassportStrategy();
};

const registerRouters = (app: Express) => {
    app.use(express.static("public"));
    app.use(Routes.USER, routes.userRouter);
    app.use(Routes.AUTH, routes.authRouter);
    app.use(Routes.POST, routes.postRouter);
    app.use(Routes.KEY, routes.keyRouter);
    app.use(Routes.FILE, routes.fileRouter);
};

const registerErrorHandlers = (app: Express) => {
    app.use(notFoundErrorHandler);
    app.use(errorHandler);
};

const registerCronJobs = () => {
    cron.schedule("0 0 * * *", () => {
        expireUsers().catch((error) => {
            consola.error(Log.CRON_JOB_ERROR, error);
        });
    });
};

const onServerListening = (address: AddressInfo) => {
    consola.ready(Log.APP_START, address.port);
};

const onConnectionAchieved = async (): Promise<void> => {
    const app = express();
    registerMiddlewares(app);
    registerRouters(app);

    const server = app.listen(portNum);
    const address = server.address();

    server.on("listening", () => onServerListening(address as AddressInfo));

    registerCronJobs();
    registerErrorHandlers(app);
};

const onConnectionFailed = (error: string | Error | undefined): void => {
    consola.error(Log.DB_CONNECTION_ERROR, error);
};

const start = (): void => {
    createConnection().then(onConnectionAchieved).catch(onConnectionFailed);
};

start();

import { executeRequest } from "./utils";
import { IRequestHandler } from "../types";
import userController from "./userController";
import authController from "./authController";
import postController from "./postController";
import keyController from "./keyController";
import fileController from "./fileController";

const controllers: Record<string, unknown> = {
    user: userController,
    auth: authController,
    post: postController,
    key: keyController,
    file: fileController,
};

const wrappedControllers = Object.keys(controllers).reduce(
    (controllerAccum: Record<string, unknown>, controllerCurr: string) => {
        const controller = controllers[controllerCurr] as Record<
            string,
            unknown
        >;
        controllerAccum[controllerCurr] = Object.keys(controller).reduce(
            (childAccum: Record<string, unknown>, childCurr: string) => {
                const childController = controller[
                    childCurr
                ] as IRequestHandler;
                childAccum[childCurr] = executeRequest(childController);
                return childAccum;
            },
            {},
        );
        return controllerAccum;
    },
    {},
);

export default wrappedControllers;

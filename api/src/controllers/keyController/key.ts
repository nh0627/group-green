const getKey = async (): Promise<{
    key: string | undefined;
}> => {
    const key = {
        key: process.env.MAP_API_KEY,
    };
    return key;
};

export default {
    starter: getKey,
};

import signin from "./signin";
import signout from "./signout";
import token from "./token";

const authController = {
    signin,
    signout,
    token,
};

export default authController;

import { Request } from "express";
import { getRepository } from "typeorm";
import { HttpError } from "http-errors";
import { JwtPayload } from "jsonwebtoken";
import {
    verifyToken,
    createBadRequestError,
    isError,
    createUnauthorizedError,
} from "../../utils";
import { issueAccessToken } from "../utils";
import { Token, User } from "../../entities";
import { ACCESS_TOKEN_EXPIRES_IN } from "../../constants";

const updateAuth = async (user: User) => {
    const accessToken = issueAccessToken(user);
    return {
        auth: {
            accessToken,
            accessTokenExpiresIn: ACCESS_TOKEN_EXPIRES_IN,
        },
    };
};

const isSameRefreshToken = (
    refreshTokenFromResponse: string,
    refreshTokenFromDB: string,
    user: User,
) =>
    refreshTokenFromResponse === refreshTokenFromDB
        ? updateAuth(user)
        : createBadRequestError();

const findRefreshToken = async (
    refreshToken: string,
    user: User,
): Promise<unknown> => {
    const result = await getRepository(Token)
        .findOneOrFail({ user })
        .catch(createUnauthorizedError);
    return isError(result)
        ? result
        : isSameRefreshToken(refreshToken, result?.token, user);
};

const verifyRefreshToken = async (
    refreshToken: string,
    user: User,
): Promise<unknown> => {
    const result = verifyToken(refreshToken);
    return isError(result) ? result : findRefreshToken(refreshToken, user);
};

const findUser = (req: Request, jwtPayload: JwtPayload) => {
    const user = jwtPayload.user as User;
    const refreshToken = (req.query.refreshToken as string) || "";
    return getRepository(User)
        .findOneOrFail({ id: user.id })
        .then((user) => verifyRefreshToken(refreshToken, user))
        .catch(createUnauthorizedError);
};

const verifyAcccessToken = async (req: Request): Promise<unknown> => {
    const accessToken = req.headers.authorization?.slice(7);
    const result = verifyToken(accessToken as string);
    return isError(result) ? result : findUser(req, result as JwtPayload);
};

const hasAccessToken = (req: Request): Promise<unknown> | HttpError =>
    req.headers.authorization
        ? verifyAcccessToken(req)
        : createUnauthorizedError();

export default {
    starter: hasAccessToken,
    requiredQueryParams: ["refreshToken"],
};

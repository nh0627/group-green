import { Request, Response } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import jwt from "jsonwebtoken";
import { User } from "../../entities";
import { createAndValidateEntity, issueAccessToken } from "../utils";
import { isError, verifyToken } from "../../utils";
import { Token } from "../../entities";
import { HttpError } from "http-errors";
import { TOKEN_ISSUER, ACCESS_TOKEN_EXPIRES_IN } from "../../constants";

const refreshTokenExpiresIn = "14d";
const issueRefreshToken = (user: User) =>
    jwt.sign({ user }, process.env.JWT_SECRET as string, {
        expiresIn: refreshTokenExpiresIn,
        issuer: TOKEN_ISSUER,
    });

const saveRefreshToken = async (token: Token) =>
    await getRepository(Token).save(token);

const createRefreshTokenEntity = async (user: User) => {
    const refreshTokenInfo = { user, token: issueRefreshToken(user) };
    return await createAndValidateEntity(refreshTokenInfo, Token).then(
        (result) => saveRefreshToken(result as Token),
    );
};

const deleteRefreshToken = async (user: User) =>
    await getRepository(Token)
        .delete({ user })
        .then(() => createRefreshTokenEntity(user));

const verifyRefreshToken = async (
    refreshToken: string,
    user: User,
): Promise<unknown> => {
    const result = verifyToken(refreshToken);
    return isError(result)
        ? deleteRefreshToken(user)
        : { token: refreshToken, user };
};

const findRefreshToken = async (user: User) => {
    const result = await getRepository(Token).findOne({
        where: { user },
        relations: ["user"],
    });
    return result !== undefined
        ? verifyRefreshToken(result.token, user)
        : createRefreshTokenEntity(user);
};

const activateUser = async (user: User) => {
    const activatedUser = getRepository(User).merge(user, {
        active: true,
    });
    return await getRepository(User)
        .save(activatedUser)
        .then((user) => findRefreshToken(user));
};

const updateUser = async (user: User) => {
    const result = ((await activateUser(user)) as Token) || HttpError;
    const accessToken = issueAccessToken(user);
    return {
        user: result.user,
        auth: {
            accessToken,
            refreshToken: result.token,
            accessTokenExpiresIn: ACCESS_TOKEN_EXPIRES_IN,
            refreshTokenExpiresIn,
        },
    };
};

const signin = async (req: Request, res: Response): Promise<unknown> =>
    new Promise((resolve) => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        passport.authenticate("local", { session: false }, async (err, user) =>
            err ? resolve(err) : resolve(updateUser(user)),
        )(req, res);
    });

export default {
    starter: signin,
    requiredBodyFields: ["email", "password"],
};

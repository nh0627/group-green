import { Request } from "express";
import { Token, User } from "../../entities";
import { getRepository } from "typeorm";

const deactivateUser = async (user: User) => {
    const deactivatedUser = getRepository(User).merge(user, {
        active: false,
    });
    return await getRepository(User).save(deactivatedUser);
};

const deleteRefreshToken = async (req: Request): Promise<User> => {
    const user = req.user as User;
    return await getRepository(Token)
        .delete({ user })
        .then(() => deactivateUser(user));
};

export default {
    starter: deleteRefreshToken,
};

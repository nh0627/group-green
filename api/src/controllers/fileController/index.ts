import upload from "./upload";

const fileController = {
    upload,
};

export default fileController;

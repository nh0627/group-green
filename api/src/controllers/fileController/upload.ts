import multer from "multer";
import { Request, Response } from "express";
import { createInternalServerError } from "../../utils";

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "public");
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    },
});

const uploadHandler = multer({ storage }).single("file");

const upload = async (req: Request, res: Response): Promise<unknown> =>
    new Promise((resolve) =>
        uploadHandler(req, res, (err) =>
            err ? resolve(createInternalServerError()) : resolve(req.file),
        ),
    );

export default {
    starter: upload,
};

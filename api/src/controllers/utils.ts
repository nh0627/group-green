import { Request, Response, NextFunction } from "express";
import { validate, ValidationError } from "class-validator";
import nodemailer from "nodemailer";
import { getRepository, EntityTarget } from "typeorm";
import jwt from "jsonwebtoken";
import { IEmailOption, IRequestHandler } from "../types";
import { Hashtag, User } from "../entities";
import { TOKEN_ISSUER, ACCESS_TOKEN_EXPIRES_IN } from "../constants";
import {
    createBadRequestError,
    createOkResponse,
    createConflictError,
    createInternalServerError,
    createNoContentResponse,
    isError,
} from "../utils";

export const hasRequiredProps = (
    body: Record<string, unknown>,
    list: string[],
): boolean => {
    const isExist = (obj: Record<string, unknown>, name: string) =>
        obj.hasOwnProperty(name) && obj[name] !== null;

    const isSomeExist = (body: Record<string, unknown>, list: string[]) =>
        list.some((name) => isExist(body, name));

    return list.every((element) =>
        typeof element === "string"
            ? isExist(body, element)
            : isSomeExist(body, element),
    );
};

export const executeRequest =
    (handler: IRequestHandler) =>
    async (
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<unknown> => {
        const {
            starter,
            requiredBodyFields = [],
            requiredQueryParams = [],
        } = handler;

        const result = hasRequiredProps(req.body, requiredBodyFields)
            ? hasRequiredProps(req.query, requiredQueryParams)
                ? await starter(req, res, next).catch(createInternalServerError)
                : createBadRequestError()
            : createBadRequestError();

        return isError(result)
            ? next(result)
            : result
            ? res.send(createOkResponse(result as Record<string, unknown>))
            : res.send(createNoContentResponse());
    };

export const createAndValidateEntity = async (
    info: Record<string, unknown>,
    Entity: EntityTarget<unknown>,
): Promise<unknown> => {
    const validateEntity = async (obj: unknown) => {
        const validationErrors = await validate(obj as Record<string, unknown>);
        const isValid = validationErrors.length === 0;
        const firstValidationError = validationErrors[0];

        const makeValidationErrors = (validationError: ValidationError) => {
            const constraints = validationError.constraints as Record<
                string,
                unknown
            >;
            const firstContraint = Object.keys(constraints)[0];
            const isDuplicationError =
                firstContraint === "IsNotDuplicatedConstraint";
            return isDuplicationError
                ? createBadRequestError()
                : createConflictError();
        };

        return !isValid ? makeValidationErrors(firstValidationError) : obj;
    };

    const createdEntity = getRepository(Entity).create(info);
    return await validateEntity(createdEntity);
};

export const sendEmail = async (options: IEmailOption): Promise<unknown> => {
    const user = process.env.NODEMAILER_EMAIL;
    const pass = process.env.NODEMAILER_PASSWORD;
    const emailOptions = { ...options, from: user };

    const transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false,
        auth: {
            user,
            pass,
        },
    });

    return new Promise((resolve) =>
        transporter.sendMail(emailOptions, (error, info) =>
            error ? resolve(createInternalServerError()) : resolve(info),
        ),
    );
};

export const issueAccessToken = (user: User): string =>
    jwt.sign({ user }, process.env.JWT_SECRET as string, {
        expiresIn: ACCESS_TOKEN_EXPIRES_IN,
        issuer: TOKEN_ISSUER,
    });

export const saveAndGetHashtags = (hashtags: string[]): Promise<Hashtag[]> => {
    const saveHashtag = async (hashtag: Hashtag) =>
        await getRepository(Hashtag).save(hashtag);

    const createHashtagEntity = async (hashtag: string) =>
        await createAndValidateEntity({ name: hashtag }, Hashtag).then(
            (result) => saveHashtag(result as Hashtag),
        );

    const findHashtag = async (hashtag: string) =>
        await getRepository(Hashtag).findOne({
            where: { name: hashtag },
        });

    const checkHashtags = async (hashtags: string[]): Promise<Hashtag[]> => {
        const hashtagPromises = hashtags?.map(async (hashtag) => {
            const foundHashtag = await findHashtag(hashtag);
            return foundHashtag !== undefined
                ? foundHashtag
                : createHashtagEntity(hashtag);
        });

        return await Promise.all(hashtagPromises);
    };

    return checkHashtags(hashtags);
};

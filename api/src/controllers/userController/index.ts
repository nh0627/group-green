import save from "./save";
import confirm from "./confirm";
import getOne from "./getOne";
import update from "./update";
import likedList from "./likedList";

const userController = {
    save,
    confirm,
    getOne,
    likedList,
    update,
};

export default userController;

import { Request } from "express";
import { getRepository } from "typeorm";
import consola from "consola";
import { User, Confirm } from "../../entities";
import { HttpError } from "http-errors";
import { createAndValidateEntity } from "../utils";
import {
    encryptString,
    getRandomHash,
    createBadRequestError,
} from "../../utils";
import { IEmailOption, ILogin } from "../../types";
import { sendEmail } from "../utils";
import { Log } from "../../constants";

const deleteUserWhenConfirmFailed = async (error: HttpError, id: number) =>
    await getRepository(User)
        .delete(id)
        .then(() => error);

const sendSignupConfirmEmail = async (
    email: string,
    username: string,
    key: string,
) => {
    const clientUrl = process.env.CLIENT_URL;
    const confirmUrl = `${clientUrl as string}/confirm/key?=${key}`;
    const options: IEmailOption = {
        subject: `@${username}! Please confirm your email😀`,
        text: `Hey @${username}! Please click the link to confirm your email: ${confirmUrl}`,
        to: email,
    };

    consola.info(Log.CONFIRMATION_URL, confirmUrl);

    return await sendEmail(options);
};

const getUserInfoAndSendEmail = async (user: User, key: string) =>
    await sendSignupConfirmEmail(user.email, user.username, key)
        .then(() => ({ user }))
        .catch((error) => deleteUserWhenConfirmFailed(error, user.id));

const saveConfirm = async (confim: Confirm) =>
    await getRepository(Confirm)
        .save(confim)
        .then((result) => getUserInfoAndSendEmail(result.user, result.key));

const createConfirmEntity = async (user: User) => {
    const confirmKey = getRandomHash();
    const confirmInfo = { user, key: confirmKey };
    return await createAndValidateEntity(confirmInfo, Confirm).then((result) =>
        saveConfirm(result as Confirm),
    );
};

const deletePasswordProp = (user: User) => {
    const userInfo = { ...user };
    delete userInfo.password;
    return userInfo;
};

const saveUser = async (user: User) =>
    await getRepository(User)
        .save(user)
        .then((user) => {
            const newUser = deletePasswordProp(user);
            return createConfirmEntity(newUser as User);
        });

const createUserEntity = async (user: User) => {
    const hashedPassword = encryptString(user.password as string);
    const userWithHashedPassword = { ...user, password: hashedPassword };
    return await createAndValidateEntity(userWithHashedPassword, User).then(
        (result) => saveUser(result as User),
    );
};

const validPassword = async (
    req: Request,
): Promise<
    | {
          user: User;
      }
    | HttpError
> => {
    const { password } = req.body as ILogin;
    const minLength = 5;
    const maxLength = 24;

    const isPasswordValid = (password: string) =>
        password.length >= minLength && password.length <= maxLength;

    return isPasswordValid(password)
        ? createUserEntity(req.body as User)
        : createBadRequestError();
};

export default {
    starter: validPassword,
    requiredBodyFields: ["email", "username", "password"],
};

import { Request } from "express";
import { getRepository } from "typeorm";
import { User } from "../../entities";

const findPosts = async (req: Request): Promise<User | undefined> => {
    const user = req.user as User;

    const result = await getRepository(User).findOne({
        where: { id: user.id },
        relations: ["liked"],
    });

    return result;
};

export default {
    starter: findPosts,
};

import { Request } from "express";
import { getRepository } from "typeorm";
import { createNotFoundError } from "../../utils";
import { User } from "../../entities";
import { HttpError } from "http-errors";

const getUser = async (req: Request): Promise<User | HttpError> => {
    const username = req.params.username;

    return await getRepository(User)
        .findOneOrFail({
            username,
        })
        .catch(createNotFoundError);
};

export default {
    starter: getUser,
};

import { Request } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import dayjs from "dayjs";
import { User } from "../../entities";
import { createBadRequestError, createNotFoundError } from "../../utils";
import { HttpError } from "http-errors";

const apiUrl = process.env.API_URL as string;

const saveUser = async (user: User) => await getRepository(User).save(user);

const mergeUser = async (id: number, newData: User) =>
    getRepository(User)
        .findOneOrFail({ id })
        .then(async (foundUser) => {
            const mergedUser = getRepository(User).merge(foundUser, {
                ...newData,
                modifiedAt: dayjs().toDate(),
            });
            return await saveUser(mergedUser);
        })
        .catch(createNotFoundError);

const validateUser = async (req: Request): Promise<User | HttpError> => {
    const { id } = req.user as User;
    const newData = req.body as User;

    if (newData.imgUrl) newData.imgUrl = `${apiUrl}/${newData.imgUrl}`;

    const validateErrors = await validate(newData);
    return validateErrors.length === 0
        ? await mergeUser(id, newData)
        : createBadRequestError();
};

export default {
    starter: validateUser,
    requiredBodyFields: ["username"],
};

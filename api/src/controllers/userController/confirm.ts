import { Request } from "express";
import { getRepository, MoreThan } from "typeorm";
import dayjs from "dayjs";
import { User, Confirm } from "../../entities";
import { createNotFoundError } from "../../utils";

const updateUser = async (user: User) => {
    const confirmedUser = getRepository(User).merge(user, {
        confirmed: true,
    });
    return await getRepository(User).save(confirmedUser);
};

const deleteConfirm = async (confirm: Confirm) => {
    const relatedUser = confirm.user;
    return await getRepository(Confirm)
        .delete(confirm)
        .then(() => updateUser(relatedUser));
};

const findConfirm = async (req: Request): Promise<unknown> => {
    // eslint-disable-next-line
    const aDayAgo = dayjs().subtract(1, "day").toISOString();

    const { key } = req.body as { key: string };
    return await getRepository(Confirm)
        .findOneOrFail({
            where: { key, createdAt: MoreThan(aDayAgo) },
            relations: ["user"],
        })
        .then((confirm) => deleteConfirm(confirm))
        .catch(createNotFoundError);
};

export default {
    starter: findConfirm,
    requiredBodyFields: ["key"],
};

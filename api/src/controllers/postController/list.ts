import { Request } from "express";
import { Post } from "../../entities";
import { getRepository } from "typeorm";
import { createBadRequestError } from "../../utils";
import { HttpError } from "http-errors";

const getPosts = async (condition: Record<string, unknown>) =>
    await getRepository(Post).find({
        where: condition,
        relations: ["user", "hashtags", "likes"],
    });

const getPostsWithUsername = async (username: string) =>
    await getRepository(Post)
        .createQueryBuilder("post")
        .innerJoinAndSelect("post.user", "user")
        .leftJoinAndSelect("post.hashtags", "hashtags")
        .leftJoinAndSelect("post.likes", "likes")
        .where("user.username = :username", { username })
        .getMany();

const getPostsWithHashtag = async (hashtag: string) =>
    await getRepository(Post)
        .createQueryBuilder("post")
        .innerJoinAndSelect("post.user", "user")
        .leftJoinAndSelect("post.likes", "likes")
        .innerJoin("post.hashtags", "hashtag", "hashtag.name = :hashtag", {
            hashtag,
        })
        .getMany();

const setFilterConditions = async (
    req: Request,
): Promise<Post[] | HttpError> => {
    const { query } = req;
    const result: { posts: Promise<Post[]> } = {
        posts: {} as Promise<Post[]>,
    };

    switch (query.filterby) {
        case "userId":
            const condition = { user: { id: query.value } };
            result.posts = getPosts(condition);
            break;
        case "username":
            result.posts = getPostsWithUsername(query.value as string);
            break;
        case "hashtag":
            result.posts = getPostsWithHashtag(query.value as string);
            break;
        default:
            return createBadRequestError();
    }

    return await result.posts;
};

export default {
    starter: setFilterConditions,
};

import { Request } from "express";
import { getRepository } from "typeorm";
import { plainToClass } from "class-transformer";
import { validate } from "class-validator";
import { Post, User } from "../../entities";
import { createBadRequestError, createNotFoundError } from "../../utils";
import { saveAndGetHashtags } from "../utils";
import { HttpError } from "http-errors";

const savePost = async (post: Post) => await getRepository(Post).save(post);

const validateAndSavePost = async (mergedPost: Post) => {
    const validateErrors = await validate(mergedPost);
    return validateErrors.length === 0
        ? await savePost(mergedPost)
        : createBadRequestError();
};

const updatePost = async (req: Request): Promise<Post | HttpError> => {
    const id = parseInt(req.params.id);
    const user = req.user as User;
    const newPost: Post = plainToClass(Post, req.body as Post);

    if (newPost.hashtags !== undefined && newPost.hashtags.length > 0)
        newPost.hashtags = await saveAndGetHashtags(
            newPost.hashtags as string[],
        );

    return getRepository(Post)
        .findOneOrFail({ id, user })
        .then(async (foundPost) => {
            const mergedPost = getRepository(Post).merge(foundPost, {
                ...newPost,
                createdAt: foundPost.createdAt,
            });
            return await validateAndSavePost(mergedPost);
        })
        .catch(createNotFoundError);
};

export default {
    starter: updatePost,
};

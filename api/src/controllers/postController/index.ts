import save from "./save";
import remove from "./remove";
import list from "./list";
import update from "./update";
import getOne from "./getOne";
import like from "./like";

const postController = {
    save,
    remove,
    list,
    update,
    getOne,
    like,
};

export default postController;

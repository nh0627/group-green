import { Request } from "express";
import { HttpError } from "http-errors";
import { getRepository } from "typeorm";
import { Post, User } from "../../entities";
import { createNotFoundError } from "../../utils";

const savePost = async (post: Post, newLikes: User[]) => {
    post.likes = newLikes;
    return await getRepository(Post).save(post);
};

const addLike = async (user: User, post: Post) => {
    const newLikes = [...post.likes, user];
    return savePost(post, newLikes);
};

const deleteLike = async (currUser: User, post: Post) => {
    const newLikes = post.likes.filter((user) => currUser.id !== user.id);
    return savePost(post, newLikes);
};

const findUser = async (currUser: User, post: Post) => {
    const result = post.likes.find((user) => currUser.id === user.id);
    return !result ? addLike(currUser, post) : deleteLike(currUser, post);
};

const findPost = async (req: Request): Promise<Post | HttpError> => {
    const id = parseInt(req.params.id);
    const { user } = req;

    return await getRepository(Post)
        .findOneOrFail({
            where: { id },
            relations: ["likes"],
        })
        .then((foundPost) => findUser(user as User, foundPost))
        .catch(createNotFoundError);
};

export default {
    starter: findPost,
};

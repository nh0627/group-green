import { Request } from "express";
import { Post, User } from "../../entities";
import { getRepository } from "typeorm";

const removePost = async (req: Request): Promise<null> => {
    const user = req.user as User;
    const id = parseInt(req.params.id);
    return await getRepository(Post)
        .delete({ id, user })
        .then(() => null);
};

export default {
    starter: removePost,
};

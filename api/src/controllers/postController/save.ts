import { Request } from "express";
import { getRepository } from "typeorm";
import { Post, User } from "../../entities";
import { createAndValidateEntity, saveAndGetHashtags } from "../utils";

const apiUrl = process.env.API_URL as string;

const savePost = async (post: Post) => await getRepository(Post).save(post);

const createPostEntity = async (post: Post, user: User) => {
    const postWithUser = { ...post, url: `${apiUrl}/${post.url}`, user };
    return await createAndValidateEntity(postWithUser, Post).then((result) =>
        savePost(result as Post),
    );
};

const hasHashtags = async (req: Request): Promise<Post> => {
    const post = req.body as Post;
    const user = req.user as User;
    if (post.hashtags !== undefined && post.hashtags.length > 0)
        post.hashtags = await saveAndGetHashtags(post.hashtags as string[]);
    return createPostEntity(post, user);
};

export default {
    starter: hasHashtags,
    requiredBodyFields: ["url"],
};

import { Request } from "express";
import { getRepository } from "typeorm";
import { createNotFoundError } from "../../utils";
import { Post } from "../../entities";
import { HttpError } from "http-errors";

const getPostById = async (req: Request): Promise<Post | HttpError> => {
    const id = parseInt(req.params.id);

    return await getRepository(Post)
        .findOneOrFail({
            where: { id },
            relations: ["user", "hashtags", "likes"],
        })
        .catch(createNotFoundError);
};

export default {
    starter: getPostById,
    requiredBodyFields: [],
};

import crypto from "crypto";
import randomstring from "randomstring";
import createError from "http-errors";
import { HttpError } from "http-errors";
import jwt from "jsonwebtoken";
import { HASH_FUNCTION, HEX } from "./constants";
import { IResponse } from "./types";

export const encryptString = (str: string): string => {
    const salt = process.env.HASH_SECRET as string;
    const hashFunction = (process.env.HASH_FUNCTION as string) || HASH_FUNCTION;
    const hashString = crypto
        .createHash(hashFunction)
        .update(`${str}${salt}`)
        .digest(HEX);
    return hashString;
};

export const getRandomHash = (): string => {
    const randomStr = randomstring.generate(128);
    return encryptString(randomStr);
};

export const createOkResponse = (body?: Record<string, unknown>): IResponse => {
    const res = { status: 200 };
    return body
        ? {
              ...res,
              body,
          }
        : res;
};

export const createNoContentResponse = (): IResponse => ({ status: 204 });

export const createInternalServerError = (): HttpError =>
    new createError.InternalServerError();

export const createBadRequestError = (): HttpError =>
    new createError.BadRequest();

export const createNotFoundError = (): HttpError => new createError.NotFound();

export const createConflictError = (): HttpError => new createError.Conflict();

export const createUnauthorizedError = (): HttpError =>
    new createError.Unauthorized();

export const isError = (obj: unknown): boolean => obj instanceof HttpError;

export const verifyToken = (token: string): string | jwt.JwtPayload => {
    try {
        return jwt.verify(token, process.env.JWT_SECRET as string);
    } catch (e) {
        return createBadRequestError();
    }
};

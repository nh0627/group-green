import {
    ErrorRequestHandler,
    RequestHandler,
    Request,
    Response,
    NextFunction,
} from "express";
import createError, { HttpError } from "http-errors";
import consola from "consola";
import passport from "passport";
import passportLocal from "passport-local";
const LocalStrategy = passportLocal.Strategy;
import passportJWT from "passport-jwt";
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
import { getRepository } from "typeorm";
import { IResponse } from "./types";
import { Log } from "./constants";
import { User } from "./entities";
import {
    encryptString,
    createUnauthorizedError,
    createInternalServerError,
} from "./utils";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorHandler: ErrorRequestHandler = (error, req, res, next) => {
    if (error === undefined) error = new createError.InternalServerError();
    const { status, message } = error as HttpError;
    consola.error(`${Log.ERROR}${message}`);
    const resObj: IResponse = { status, errorMessage: message };
    res.status(status).json(resObj);
};

export const notFoundErrorHandler: RequestHandler = (req, res, next) => {
    next(new createError.NotFound());
};

export const setPassportStrategy = (): void => {
    passport.use(
        new LocalStrategy(
            {
                usernameField: "email",
                passwordField: "password",
            },
            (username, password, done) => {
                const encryptedPassword = encryptString(password);
                getRepository(User)
                    .createQueryBuilder("user")
                    .andWhere("user.email = :email", {
                        email: username,
                    })
                    .andWhere("user.password = :password", {
                        password: encryptedPassword,
                    })
                    .andWhere("user.confirmed = :confirmed", {
                        confirmed: true,
                    })
                    .getOne()
                    .then((user) =>
                        user
                            ? done(null, user)
                            : done(createUnauthorizedError()),
                    )
                    .catch(() => done(createInternalServerError()));
            },
        ),
    );
    passport.use(
        new JWTStrategy(
            {
                jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
                secretOrKey: process.env.JWT_SECRET,
            },
            (jwtPayload, done) => {
                // eslint-disable-next-line
                const { id } = jwtPayload.user as User;
                getRepository(User)
                    .findOneOrFail({
                        where: { id },
                    })
                    .then((user) =>
                        user
                            ? done(null, user)
                            : done(createUnauthorizedError()),
                    )
                    .catch(() => {
                        done(createUnauthorizedError());
                    });
            },
        ),
    );
};

export const checkAuth = (
    req: Request,
    res: Response,
    next: NextFunction,
): void => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    passport.authenticate("jwt", { session: false }, async (err, user) => {
        req.user = user as User;
        return user ? next() : next(err || createUnauthorizedError());
    })(req, res);
};

import { Request, Response, NextFunction, Application } from "express";

export interface IResponse {
    status: number;
    errorMessage?: string;
    body?: Record<string, unknown>;
}

export interface IEmailOption {
    subject: string;
    text: string;
    to: string;
    from?: string;
}

export interface IRequestHandler {
    starter: (
        req: Request,
        res?: Response,
        next?: NextFunction,
    ) => Promise<unknown>;
    requiredBodyFields?: string[];
    requiredQueryParams?: string[];
}

export interface ILogin {
    email?: string;
    username?: string;
    password: string;
}

export interface IAuthController {
    signin: Application;
    signout: Application;
    token: Application;
}

export interface IKeyController {
    key: Application;
}

export interface IFileController {
    upload: Application;
}

export interface IDefaultController {
    getOne: Application;
    list: Application;
    save: Application;
    remove: Application;
    update: Application;
}

export interface IPostController extends IDefaultController {
    like: Application;
}

export interface ILikeController extends IDefaultController {}

export interface IUserController extends IDefaultController {
    confirm: Application;
    likedList: Application;
}

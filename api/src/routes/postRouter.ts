import express from "express";
import { checkAuth } from "../middlewares";
import controllers from "../controllers";
import { Routes } from "../constants";
import { IPostController } from "../types";

const router = express.Router();
const postController = controllers.post as IPostController;

router.get(Routes.DEFAULT, postController.list);
router.post(Routes.DEFAULT, checkAuth, postController.save);
router.delete(Routes.ID, checkAuth, postController.remove);
router.put(Routes.ID, checkAuth, postController.update);
router.get(Routes.ID, postController.getOne);

router.post(`${Routes.ID}${Routes.LIKE}`, checkAuth, postController.like);

export default router;

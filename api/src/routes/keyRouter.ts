import express from "express";
import controllers from "../controllers";
import { Routes } from "../constants";
import { IKeyController } from "../types";

const router = express.Router();
const keyController = controllers.key as IKeyController;

router.get(Routes.DEFAULT, keyController.key);

export default router;

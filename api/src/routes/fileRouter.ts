import express from "express";
import controllers from "../controllers";
import { checkAuth } from "../middlewares";
import { Routes } from "../constants";
import { IFileController } from "../types";

const router = express.Router();
const fileController = controllers.file as IFileController;

router.post(Routes.DEFAULT, checkAuth, fileController.upload);

export default router;

import express from "express";
import { checkAuth } from "../middlewares";
import controllers from "../controllers";
import { Routes } from "../constants";
import { IAuthController } from "../types";
const router = express.Router();

const authController = controllers.auth as IAuthController;

router.post(Routes.SIGNIN, authController.signin);
router.post(Routes.SIGNOUT, checkAuth, authController.signout);
router.get(Routes.TOKEN, authController.token);

export default router;

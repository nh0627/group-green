import express from "express";
import controllers from "../controllers";
import { Routes } from "../constants";
import { IUserController } from "../types";
import { checkAuth } from "../middlewares";

const router = express.Router();
const userController = controllers.user as IUserController;

router.get(Routes.USERNAME, userController.getOne);
router.get(`${Routes.ID}${Routes.LIKE}`, checkAuth, userController.likedList);
router.post(Routes.DEFAULT, userController.save);
router.put(Routes.CONFIRM, userController.confirm);
router.put(Routes.DEFAULT, checkAuth, userController.update);

export default router;

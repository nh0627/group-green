export const PORT = "5000";
export const HASH_FUNCTION = "sha512";
export const HEX = "hex";
export const TOKEN_ISSUER = "green";
export const ACCESS_TOKEN_EXPIRES_IN = "1h";

export enum Log {
    APP_START = "App is listening on: ",
    ERROR = "Error: ",
    DB_CONNECTION_ERROR = "DB Connection Error: ",
    CRON_JOB_ERROR = "Cron Job Error: ",
    CRON_JOB = "Cron Job: ",
    CONFIRMATION_URL = "Confirmation URL: ",
}

export enum Routes {
    DEFAULT = "/",
    ID = "/:id",
    USERNAME = "/:username",
    USER = "/users",
    AUTH = "/auth",
    POST = "/posts",
    LIKE = "/likes",
    KEY = "/clientKeys",
    FILE = "/upload",
    CONFIRM = "/confirm",
    SIGNIN = "/signin",
    SIGNOUT = "/signout",
    TOKEN = "/token",
    PUBLIC = "/public",
}

export enum ErrorMessage {
    DUPLICATION = "$value already exists. Choose another $property.",
}

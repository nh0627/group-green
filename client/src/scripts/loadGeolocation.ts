import axios from "axios";
import Geocode from "react-geocode";
import { baseURL, Routes } from "./../constants";

const loadGeolocation = (callback: { (): void; (): void }): void => {
    const existingScript = document.getElementById("googleGeolocation");

    if (!existingScript) {
        const script = document.createElement("script");

        const getKey = async () =>
            await axios.get(baseURL + Routes.geolocationKey).then((response) => {
                Geocode.setApiKey(response.data.body.key);
                return response.data.body.key as string;
            });

        void getKey()
            .then((key) => {
                script.src = `https://maps.googleapis.com/maps/api/js?key=${key}&libraries=places`;
                script.id = "googleGeolocation";
                document.body.appendChild(script);
            })
            .catch(() => "");

        script.onload = () => {
            if (callback) callback();
        };
    }
    if (existingScript && callback) callback();
};
export default loadGeolocation;

export const baseURL = "http://localhost:5000";

export enum Routes {
    signIn = "/auth/signin",
    signOut = "/auth/signout",
    geolocationKey = "/clientKeys",
    users = "/users",
    posts = "/posts",
    upload = "/upload",
}

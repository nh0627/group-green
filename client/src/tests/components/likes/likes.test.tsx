import React from "react";
import { shallow, mount } from "enzyme";
import axios from "axios";
import Like, { IUseLike } from "./../../../components/likes/Like";
import userContext from "./../../../components/context/UserContext";
import { waitFor } from "@testing-library/react";
import { IPostObject } from "./../../../types/types";
import { baseURL, Routes } from "../../../constants";

const user = {
    active: true,
    confirmed: true,
    createdAt: "2021-07-13T10:56:05.242Z",
    description: "Hello world",
    email: "example@gmail.com",
    id: 41,
    imgUrl: "https://i.pinimg.com/550x/80/9b/6b/809b6b0111f1053e152858b34fafa438.jpg",
    modifiedAt: "2021-07-13T10:56:05.242Z",
    nickname: "Starlink",
    username: "test",
};

const post: IPostObject = {
    createdAt: "2021-08-26T06:33:30.394Z",
    description: "hello world!",
    id: 95,
    location: "Dubai - United Arab Emirates",
    modifiedAt: "2021-08-26T11:10:24.754Z",
    url: "http://localhost:5000/21168405ba71388a2fed9cc5b3af8e43.png",
    hashtags: [],
    user,
    likes: [
        {
            active: true,
            confirmed: true,
            createdAt: "2021-07-23T07:06:55.480Z",
            description: "Sunny day",
            email: "test2@test.com",
            id: 55,
            imgUrl: "https://static.zerochan.net/Pikachu.full.1452733.jpg",
            modifiedAt: "2021-07-23T07:06:55.480Z",
            nickname: "Fred",
            username: "testtest2",
        },
    ],
};

const props = { post, setMessageType: () => jest.fn() };

const setUp = (props: IUseLike) =>
    mount(
        <userContext.Provider value={{ user }}>
            <Like {...props} />
        </userContext.Provider>,
    );

describe("like component testing", () => {
    beforeEach(() => {
        jest.spyOn(axios, "post");
        jest.spyOn(window.localStorage.__proto__, "getItem");
        window.localStorage.__proto__.getItem = jest
            .fn()
            .mockImplementation(
                () =>
                    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImNyZWF0ZWRBdCI6IjIwMjEtMDctMTNUMTA6NTY6MDUuMjQyWiIsImNvbmZpcm1lZCI6dHJ1ZSwiYWN0aXZlIjp0cnVlLCJtb2RpZmllZEF0IjoiMjAyMS0wNy0xM1QxMDo1NjowNS4yNDJaIiwiaWQiOjQxLCJlbWFpbCI6ImV4YW1wbGVAZ21haWwuY29tIiwidXNlcm5hbWUiOiJ0ZXN0Iiwibmlja25hbWUiOiJTdGFybGluayIsImRlc2NyaXB0aW9uIjoiSGVsbG8gd29ybGQiLCJpbWdVcmwiOiJodHRwczovL2kucGluaW1nLmNvbS81NTB4LzgwLzliLzZiLzgwOWI2YjAxMTFmMTA1M2UxNTI4NThiMzRmYWZhNDM4LmpwZyJ9LCJpYXQiOjE2Mjk3MDE4NzQsImV4cCI6MTYyOTcwNTQ3NCwiaXNzIjoiZ3JlZW4ifQ.wuS4Wb8xD5_QwoBsmvx_eYkSvU-BqVQJr-eiINvrVfA",
            );

        jest.clearAllMocks();
    });

    it("Like component should render correctly if user logged in", () => {
        shallow(
            <userContext.Provider value={{ user }}>
                <Like {...props} />
            </userContext.Provider>,
        );
    });

    it("When user click on like axios.post will be called", async () => {
        const postId = post?.id;
        const mockPost = jest.fn().mockImplementation(() => Promise.resolve());
        axios.post = mockPost;
        const component = setUp(props);
        component.find("#like").simulate("click");
        await waitFor(() => expect(mockPost).toHaveBeenCalledTimes(1));
        await waitFor(() =>
            expect(mockPost).toHaveBeenCalledWith(`${baseURL}${Routes.posts}/${postId}/likes`, "", {
                headers: {
                    Authorization:
                        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImNyZWF0ZWRBdCI6IjIwMjEtMDctMTNUMTA6NTY6MDUuMjQyWiIsImNvbmZpcm1lZCI6dHJ1ZSwiYWN0aXZlIjp0cnVlLCJtb2RpZmllZEF0IjoiMjAyMS0wNy0xM1QxMDo1NjowNS4yNDJaIiwiaWQiOjQxLCJlbWFpbCI6ImV4YW1wbGVAZ21haWwuY29tIiwidXNlcm5hbWUiOiJ0ZXN0Iiwibmlja25hbWUiOiJTdGFybGluayIsImRlc2NyaXB0aW9uIjoiSGVsbG8gd29ybGQiLCJpbWdVcmwiOiJodHRwczovL2kucGluaW1nLmNvbS81NTB4LzgwLzliLzZiLzgwOWI2YjAxMTFmMTA1M2UxNTI4NThiMzRmYWZhNDM4LmpwZyJ9LCJpYXQiOjE2Mjk3MDE4NzQsImV4cCI6MTYyOTcwNTQ3NCwiaXNzIjoiZ3JlZW4ifQ.wuS4Wb8xD5_QwoBsmvx_eYkSvU-BqVQJr-eiINvrVfA",
                },
            }),
        );
    });
});

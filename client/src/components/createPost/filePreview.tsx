import React, { useState } from "react";
import { Form } from "semantic-ui-react";
import { IFilePreview } from "../../types/types";
import axios, { AxiosResponse } from "axios";
import { baseURL, Routes } from "./../../constants";
import { PostImageContainer } from "./StyledPost";

const ImgPrev = (setError: React.Dispatch<React.SetStateAction<string>>): IFilePreview => {
    const [{ src, name }, setImg] = useState({ src: "./images/placeholder.jpg", name: "" });
    const [authKey] = useState(localStorage.getItem("token"));

    const downloadImageToServer = (file: File) => {
        const createImageFile = new FormData();
        createImageFile.append("file", file);

        void axios
            .post(baseURL + Routes.upload, createImageFile, {
                headers: {
                    Authorization: `Bearer ${authKey as string}`,
                },
            })
            .then((res: AxiosResponse) => {
                setImg({
                    src: URL.createObjectURL(file),
                    name: res.data.body.filename as string,
                });
            })
            .catch(() => setError("Something went wrong"));
    };

    const uploadFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        const allowedImageExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if (!event.target.files) return;
        const file = event.target.files[0];

        return !allowedImageExtensions.test(file?.name)
            ? (setError("Invalid image format"), setImg({ src: "", name: "" }))
            : (setError(""), downloadImageToServer(file));
    };

    const ShowImage = () => (
        <div>
            <PostImageContainer src={src} />
            <Form.Input
                type="file"
                accept="image/*"
                onChange={uploadFile}
                encType="multipart/form-data"
            />
        </div>
    );

    return { ShowImage, name };
};

export default ImgPrev;

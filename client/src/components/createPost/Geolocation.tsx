import usePlacesAutocomplete from "use-places-autocomplete";
import React from "react";
import {
    Combobox,
    ComboboxInput,
    ComboboxPopover,
    ComboboxList,
    ComboboxOption,
} from "@reach/combobox";
import "@reach/combobox/styles.css";
import { usePosition } from "use-position";
import Geocode from "react-geocode";

interface IGeolocation {
    setLocation: React.Dispatch<React.SetStateAction<string>>;
}

const Geolocation = ({ setLocation }: IGeolocation): JSX.Element => {
    const watch = true;
    Geocode.setLanguage("en");
    const { latitude, longitude } = usePosition(watch);
    const {
        ready,
        value,
        suggestions: { status, data },
        setValue,
    } = usePlacesAutocomplete();

    const getLocation = () => {
        if (latitude && longitude)
            void Geocode.fromLatLng(latitude?.toString(), longitude?.toString()).then(
                (response) => {
                    setValue(response.results[0].formatted_address);
                },
            );
    };

    const renderSuggestions = () => {
        const useCurrentLocation = (
            <ComboboxOption value="📍 Use current location" onClick={() => getLocation()} />
        );
        const suggestions = data.map(({ id, description }) => (
            <ComboboxOption key={id} value={description} />
        ));
        suggestions.unshift(useCurrentLocation);
        return suggestions;
    };

    return (
        <div>
            <div
                style={{
                    fontSize: "13px",
                    fontWeight: 700,
                    marginBottom: "4px",
                }}
            >
                Location
            </div>
            <Combobox
                onSelect={(val) => {
                    setValue(val, false);
                    setLocation(val);
                }}
            >
                <ComboboxInput
                    value={value}
                    onChange={(e) => {
                        setValue(e.target.value);
                        setLocation(e.target.value);
                    }}
                    disabled={!ready}
                    style={{ marginBottom: "10px" }}
                />
                <ComboboxPopover portal={false} style={{ position: "absolute", zIndex: 100 }}>
                    <ComboboxList style={{ fontSize: "15px" }}>
                        {status === "OK" && renderSuggestions()}
                    </ComboboxList>
                </ComboboxPopover>
            </Combobox>
        </div>
    );
};

export default Geolocation;

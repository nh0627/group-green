import React from "react";
import { StyledHashtag } from "./StyledPost";

export const separateHashtags = (hashtag: string): string[] => {
    const separatedWords = hashtag.trim().split(/(?:,| )+/);
    const hashtags = separatedWords.filter(
        (word: string) => word[0] === "#" && word.length > 1 && { word },
    );
    return hashtags;
};

export const removeHashtagSymbol = (hashtags: string[]): string[] =>
    hashtags.map((hashtag: string) => hashtag.substring(1));

export const showHashtags = (hashtags: string[]): JSX.Element[] => {
    const maxHashtagAmount = 5;
    return hashtags?.length <= maxHashtagAmount
        ? hashtags
              ?.slice(0, maxHashtagAmount)
              .map((hash: string, id: number) => <StyledHashtag key={id}>{hash}</StyledHashtag>)
        : hashtags?.map((hash: string, id: number) => (
              <StyledHashtag key={id}>{hash}</StyledHashtag>
          ));
};

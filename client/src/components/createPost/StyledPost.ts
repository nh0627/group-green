import styled from "styled-components";

export const PostContainer = styled.div`
    &&& {
        display: flex;
        margin-bottom: 20px;
    }
`;

export const ImageContainer = styled.div`
    &&& {
        vwidth: 50%;
        margin-right: 20px;
    }
`;

export const HashtagsContainer = styled.div`
    &&& {
        display: flex;
        flexwrap: wrap;
    }
`;

export const PostInfoContainer = styled.div`
    &&& {
        width: 50%;
    }
`;

export const PostImageContainer = styled.img`
    &&& {
        max-width: 100%;
        max-height: 100%;
    }
`;

export const StyledHashtag = styled.span`
    &&& {
        fontsize: 15px;
        margin: 5px;
        background: rgb(3 169 244 / 61%);
        padding: 10px 20px;
        border-radius: 20px;
    }
`;

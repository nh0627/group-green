import { FormikProps, useFormik } from "formik";
import React, { useState, useEffect, useContext } from "react";
import { Form } from "semantic-ui-react";
import { ModalBody } from "./../baseModal/BaseModal";
import { removeHashtagSymbol, separateHashtags, showHashtags } from "./Hashtag";
import { INewPost, IOpen } from "../../types/types";
import Geolocation from "./Geolocation";
import loadGeolocation from "../../scripts/loadGeolocation";
import { FieldError } from "../notifications/Errors";
import { ShowNotification } from "../notifications/Notification";
import ImgPrev from "./filePreview";
import { StyledDownloadError } from "./../userPost/post/StyledNewUserPost";
import UserContext from "../context/UserContext";
import axios from "axios";
import { baseURL, Routes } from "../../constants";
import { HashtagsContainer, ImageContainer, PostContainer, PostInfoContainer } from "./StyledPost";

export interface ICreatePostProps {
    hashtags: string;
    description: string;
}

const CreatePost: React.FC<IOpen> = ({ isOpen }) => {
    const [loaded, setLoaded] = useState(false);
    const [error, setError] = useState("");
    const { setMessageType, Notification } = ShowNotification();
    const { ShowImage, name } = ImgPrev(setError);
    const [geolocation, setLocation] = useState("");
    const { user } = useContext(UserContext);
    const [authKey] = useState(localStorage.getItem("token"));

    const sendPost = () => {
        const timeBeforeRedirect = 3000;
        return authKey
            ? axios
                  .post(baseURL + Routes.posts, newPost, {
                      headers: {
                          Authorization: `Bearer ${authKey}`,
                      },
                  })
                  .then(
                      (response) =>
                          response.status === 200 &&
                          (setMessageType({
                              message: "Post successfully created",
                              type: "positive",
                          }),
                          setTimeout(
                              () => location.assign(`${user?.username || ""}`),
                              timeBeforeRedirect,
                          )),
                  )
                  .catch(() => {
                      setMessageType({
                          message: "Something went wrong",
                          type: "negative",
                      });
                  })
            : setMessageType({
                  message: "You are not logged in",
                  type: "negative",
              });
    };

    const createPostformik: FormikProps<ICreatePostProps> = useFormik({
        initialValues: {
            hashtags: "",
            description: "",
        },
        onSubmit: async () =>
            name.length === 0 ? setError("You must attach an image") : sendPost(),
    });

    const newPost: INewPost = {
        url: name,
        hashtags: removeHashtagSymbol(separateHashtags(createPostformik.values.hashtags)),
        description: createPostformik.values.description,
        location: geolocation,
    };

    useEffect(() => {
        loadGeolocation(() => {
            setLoaded(true);
        });
    }, []);

    return (
        <ModalBody
            title="Create new post"
            isOpen={isOpen}
            size="large"
            formik={createPostformik}
            buttonName="Publish"
            onSubmit={createPostformik.handleSubmit}
        >
            <PostContainer>
                <ImageContainer>
                    <ShowImage />
                    <FieldError text={error} />
                </ImageContainer>

                <PostInfoContainer>
                    <Form.Input
                        styles={{ marginBottom: "5px" }}
                        onChange={createPostformik.handleChange}
                        id="hashtags"
                        label="Hashtags"
                        value={createPostformik.values.hashtags}
                    />
                    <HashtagsContainer>
                        {showHashtags(separateHashtags(createPostformik.values.hashtags))}
                    </HashtagsContainer>

                    {loaded ? (
                        <Geolocation setLocation={setLocation} />
                    ) : (
                        <StyledDownloadError>
                            cannot download geolocation library
                        </StyledDownloadError>
                    )}
                    <Form.TextArea
                        onChange={createPostformik.handleChange}
                        id="description"
                        label="Description"
                        value={createPostformik.values.description}
                    />
                </PostInfoContainer>
            </PostContainer>
            <Notification />
        </ModalBody>
    );
};
export default CreatePost;

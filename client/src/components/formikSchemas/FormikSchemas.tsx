import * as Yup from "yup";

export const SignupSchema = Yup.object().shape({
    username: Yup.string().min(2, "Too Short!").max(50, "Too Long!").required("Required"),
    password: Yup.string()
        .min(5, "Must be at least 5 characters")
        .required("Required")
        .max(50, "Too Long!"),
    passwordConfirmation: Yup.string().oneOf([Yup.ref("password"), null], "Passwords must match"),
    email: Yup.string().email("Invalid email").required("Required"),
});

export const SignInSchema = Yup.object().shape({
    email: Yup.string().email("Invalid email").required("Required"),
    password: Yup.string().min(5, "Must be at least 5 characters").required("Required"),
});

import styled from "styled-components";

export const StyledFieldError = styled.div`
    &&& {
        color: red;
        text-align: right;
        margin: 0px;
        position: absolute;
        left: 35%;
        font-size: 10px;
    }
`;

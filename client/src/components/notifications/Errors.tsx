import React, { ReactElement } from "react";
import { useState } from "react";
import { Message } from "semantic-ui-react";
import { IErrors } from "../../types/types";
import { StyledFieldError } from "./StyledError";

export const FieldError = (text: { text: string }): ReactElement => (
    <StyledFieldError>{text.text}</StyledFieldError>
);

export const ShowError = (): IErrors => {
    const [error, setError] = useState("");

    const ErrorMessage = () => (error === "" ? null : <Message negative>{error}</Message>);

    const switchResponses = (response: string | number) =>
        response === 400
            ? setError("Email already in use. Choose another email.")
            : response === 401
            ? setError("You have entered an invalid username or password")
            : response === "no connection"
            ? setError("No internet connection")
            : setError("Something went wrong");

    return { switchResponses, setError, ErrorMessage };
};

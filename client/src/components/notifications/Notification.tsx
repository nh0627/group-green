import React from "react";
import { useState } from "react";
import { Message } from "semantic-ui-react";
import { INotification } from "../../types/types";

export const ShowNotification = (): INotification => {
    const [messageType, setMessageType] = useState({ message: "", type: "" });

    const Notification = () =>
        messageType.type === "positive" ? (
            <Message positive>{messageType.message}</Message>
        ) : messageType.type === "negative" ? (
            <Message negative>{messageType.message}</Message>
        ) : messageType.type === "none" ? null : null;

    return { setMessageType, Notification };
};

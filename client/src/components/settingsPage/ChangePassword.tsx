import React from "react";
import { Button, Form } from "semantic-ui-react";
import { StyledContainer } from "../baseModal/StyledContainer";
import SettingsNavigation from "./SetttingsNavigation";
import { ButtonsContainer } from "./StyledSettings";

const ChangePassword: React.FC = () => (
    <>
        <StyledContainer style={{ justifyContent: "flex-start" }}>
            <SettingsNavigation />
            <Form type="submit" style={{ width: "30%" }}>
                <p style={{ fontWeight: 700 }}>This is a placeholder</p>
                <Form.Input type="password" id="password" label="Change password" />
                <Form.Input type="password" id="password" label="Confirm password" />
                <ButtonsContainer>
                    <Button type="submit">Cancel</Button>
                    <Button color="green" type="submit">
                        Save
                    </Button>
                </ButtonsContainer>
            </Form>
        </StyledContainer>
    </>
);

export default ChangePassword;

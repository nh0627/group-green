import React, { useContext } from "react";
import UserContext from "../context/UserContext";
import { SettingsLink } from "./StyledSettings";
import { NavLink } from "react-router-dom";

const SettingsNavigation: React.FC = () => {
    const { user } = useContext(UserContext);

    return (
        <div style={{ width: "20%" }}>
            <SettingsLink>
                <NavLink
                    activeClassName="active"
                    key={user?.id}
                    to={"/settings/profile-settings"}
                    className="settings-link"
                >
                    <div>Edit Profile</div>
                </NavLink>
            </SettingsLink>
            <SettingsLink>
                <NavLink
                    activeClassName="active"
                    className="settings-link"
                    key={user?.id}
                    to={"/settings/password-settings"}
                >
                    <div style={{ marginTop: "10px" }}>Change password</div>
                </NavLink>
            </SettingsLink>
        </div>
    );
};

export default SettingsNavigation;

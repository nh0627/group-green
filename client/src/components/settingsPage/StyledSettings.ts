import styled from "styled-components";

export const SettingsLink = styled.div`
    &&& .settings-link {
        text-decoration: none;
        font-size: 16px;
        font-family: "Dosis", sans-serif;
        font-weight: 500;
    }

    &&& .active {
        color: green;
    }
`;

export const ButtonsContainer = styled.div`
    &&&& {
        display: flex;
        justify-content: space-between;
        margin-top: 100px;
    }
`;

export const UploadAvatar = styled.label`
    &&&& {
        font-size: 1rem;
        font-weigth: 700;
        color: white;
        padding: 10px;
        height: 20%;
        background: #16ab39;
        border-radius: 5px;
        font-weight: 700;
    }
`;

export const AvatarContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
`;

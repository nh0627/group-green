import { useFormik } from "formik";
import React, { useContext } from "react";
import { Button, Form } from "semantic-ui-react";
import { StyledContainer } from "../baseModal/StyledContainer";
import UserContext from "../context/UserContext";
import SettingsNavigation from "./SetttingsNavigation";
import { AvatarContainer, ButtonsContainer, UploadAvatar } from "./StyledSettings";

const SettingsPage: React.FC = () => {
    const { user } = useContext(UserContext);

    const updateUserInfoformik = useFormik({
        initialValues: {
            email: user?.email || "",
            username: user?.username || "",
            nickname: user?.nickname || "",
            description: user?.description || "",
        },
        onSubmit: async () => "",
        enableReinitialize: true,
    });

    return (
        <StyledContainer style={{ justifyContent: "flex-start" }}>
            <SettingsNavigation />

            <Form type="submit" style={{ width: "30%" }}>
                <p style={{ fontWeight: 700 }}>This is a placeholder</p>
                <AvatarContainer>
                    <img
                        src={user?.imgUrl || "/default-avatar.png"}
                        alt=""
                        style={{ width: "100px", borderRadius: "50px", margin: "15px 15px 15px 0" }}
                    />
                    <input type="file" id="avatar" hidden />
                    <UploadAvatar htmlFor="avatar">Change avatar</UploadAvatar>
                </AvatarContainer>

                <Form.Input
                    onChange={updateUserInfoformik.handleChange}
                    id="email"
                    label="Email"
                    value={updateUserInfoformik.values.email}
                />

                <Form.Input
                    onChange={updateUserInfoformik.handleChange}
                    id="username"
                    label="Username"
                    value={updateUserInfoformik.values.username}
                />
                <Form.Input
                    onChange={updateUserInfoformik.handleChange}
                    id="nickname"
                    label="Nickname"
                    value={updateUserInfoformik.values.nickname}
                />
                <Form.Input
                    onChange={updateUserInfoformik.handleChange}
                    id="description"
                    label="Description"
                    value={updateUserInfoformik.values.description}
                />
                <ButtonsContainer>
                    <Button type="submit">Cancel</Button>
                    <Button color="green" type="submit">
                        Save
                    </Button>
                </ButtonsContainer>
            </Form>
        </StyledContainer>
    );
};
export default SettingsPage;

import { createContext } from "react";
import { IUserObject } from "../../types/types";

interface IContextType {
    user: IUserObject | undefined;
}

const UserContext = createContext({} as IContextType);
export default UserContext;

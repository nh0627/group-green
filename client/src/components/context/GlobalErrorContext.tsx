import { createContext } from "react";

interface IContextType {
    setMessageType: React.Dispatch<
        React.SetStateAction<{
            message: string;
            type: string;
        }>
    >;
    Notification: () => JSX.Element | null;
}

const GlobalErrorContext = createContext({} as IContextType);
export default GlobalErrorContext;

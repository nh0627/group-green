import React from "react";
import { Button, Form } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import { FormikProps, useFormik } from "formik";
import { SignInSchema } from "../formikSchemas/FormikSchemas";
import { ShowPassword } from "./showPassword";
import { FieldError, ShowError } from "../notifications/Errors";
import { ModalBody } from "../baseModal/BaseModal";
import axios, { AxiosError } from "axios";
import { ITokenProps, IUser } from "./../../types/types";
import { NavLink, useHistory } from "react-router-dom";
import { baseURL, Routes } from "./../../constants";

export interface ISignInModaProps {
    email: string;
    password: string;
}

const SignInModalWindow: React.FC<ITokenProps> = ({ saveToken, saveUser, isOpen }) => {
    const { passwordShown, togglePasswordVisiblity, eye } = ShowPassword();
    const { switchResponses, setError, ErrorMessage } = ShowError();
    const history = useHistory();

    const signInFormik: FormikProps<ISignInModaProps> = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: SignInSchema as unknown,
        onSubmit: () => {
            void loginUser(user);
        },
    });

    const user = {
        email: signInFormik.values.email,
        password: signInFormik.values.password,
    };

    const loginUser = async (user: IUser) => {
        axios
            .post(baseURL + Routes.signIn, user)
            .then((response) => {
                saveUser(response.data.body.user);
                saveToken(response.data.body.auth.accessToken);
                setError("");
                history.replace(`/${response.data.body.user.username as string}`);
            })
            .catch((error: AxiosError) =>
                error.response
                    ? switchResponses(error.response.status)
                    : switchResponses("no connection"),
            );
    };

    return (
        <ModalBody
            title="Sign in"
            isOpen={isOpen}
            size="tiny"
            formik={signInFormik}
            buttonName="Login"
            onSubmit={signInFormik.handleSubmit}
        >
            <p>Under development</p>
            <Button fluid color="linkedin">
                with Google
                <img src="./icons/google.svg" width="15x" style={{ marginLeft: "5px" }} />
            </Button>
            <p
                style={{
                    display: " flex",
                    color: "gray",
                    justifyContent: "center",
                    fontSize: "18px",
                    margin: "5px",
                    fontWeight: "bold",
                }}
            >
                or
            </p>

            {signInFormik.errors.email ? <FieldError text={signInFormik.errors.email} /> : null}
            <Form.Input
                icon="envelope outline"
                onChange={signInFormik.handleChange}
                id="email"
                value={signInFormik.values.email}
                iconPosition="left"
                label="Email"
                placeholder="Email"
            />

            {signInFormik.errors.password ? (
                <FieldError text={signInFormik.errors.password} />
            ) : null}
            <div style={{ position: "relative" }}>
                <Form.Input
                    icon="lock"
                    onChange={signInFormik.handleChange}
                    type={passwordShown ? "text" : "password"}
                    id="password"
                    value={signInFormik.values.password}
                    iconPosition="left"
                    label="Password"
                    placeholder="Password"
                />
                <i
                    style={{ position: "absolute", right: "10px", bottom: "10px" }}
                    onClick={togglePasswordVisiblity}
                >
                    {eye}
                </i>
            </div>
            <div style={{ fontWeight: 700, marginBottom: "10px" }}>
                First time here?{" "}
                <NavLink exact to={"/auth/sign-up"}>
                    Sign up for GreenGram 🤗
                </NavLink>
            </div>
            <ErrorMessage />
        </ModalBody>
    );
};
export default SignInModalWindow;

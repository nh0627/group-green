import { useEffect, useState } from "react";
import { IUserObject, IUseUserReturn } from "../../types/types";

export const useUser = (): IUseUserReturn => {
    const [user, setUser] = useState<IUserObject>();

    const saveUser = (userInfo: IUserObject) => {
        localStorage.setItem("user", JSON.stringify(userInfo));
        setUser(userInfo);
    };

    useEffect(() => {
        const loggedUser = localStorage.getItem("user");

        if (loggedUser) {
            const foundUser = JSON.parse(loggedUser) as IUserObject;
            setUser(foundUser);
        }
    }, []);

    return {
        user,
        saveUser,
    };
};

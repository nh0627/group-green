import { useContext, useState } from "react";
import axios from "axios";
import GlobalErrorContext from "../context/GlobalErrorContext";
import { baseURL, Routes } from "./../../constants";

const handleSignOut = (): (() => void) => {
    const { setMessageType } = useContext(GlobalErrorContext);
    const [authKey] = useState(localStorage.getItem("token"));

    const logOut = () => {
        void axios
            .post(baseURL + Routes.signOut, "", {
                headers: {
                    Authorization: `Bearer ${authKey as string}`,
                },
            })
            .then(() => {
                localStorage.removeItem("token");
                localStorage.removeItem("user");
                setMessageType({ message: "", type: "none" });
                window.location.reload();
            })
            .catch(() => setMessageType({ message: "Unexpected error", type: "negative" }));
    };

    return logOut;
};

export default handleSignOut;

import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { IShowPassword } from "../../types/types";

export const ShowPassword = (): IShowPassword => {
    const [passwordShown, setPasswordShown] = useState(false);
    const eye = <FontAwesomeIcon icon={faEye} />;

    const togglePasswordVisiblity = () => {
        setPasswordShown(!passwordShown);
    };

    return { passwordShown, togglePasswordVisiblity, eye };
};

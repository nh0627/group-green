export const saveToken = (userToken: string): void => {
    localStorage.setItem("token", userToken);
};

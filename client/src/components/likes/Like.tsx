import axios from "axios";
import React, { useState, useContext, useEffect } from "react";
import { Popup } from "semantic-ui-react";
import { baseURL, Routes } from "../../constants";
import UserContext from "../context/UserContext";
import styles from "./../profilePage/css/mansory.module.css";
import { IPostObject, IUserObject } from "./../../types/types";

export interface IUseLike {
    post: IPostObject | undefined;
    setMessageType: React.Dispatch<
        React.SetStateAction<{
            message: string;
            type: string;
        }>
    >;
}

const Like: React.FC<IUseLike> = ({ post, setMessageType }) => {
    const [authKey] = useState(localStorage.getItem("token"));
    const [isLiked, setIsLiked] = useState<boolean>();
    const [likesCount, setLikesCount] = useState(post?.likes.length as number);
    const { user } = useContext(UserContext);
    const arrayOfLikedUsers: IUserObject[] = post?.likes as IUserObject[];
    const postId = post?.id as number;
    const currentUserId = user?.id;

    const isCurrentUserLikedPost = (): boolean =>
        arrayOfLikedUsers.some((user: IUserObject) => user.id === currentUserId);

    useEffect(() => {
        setIsLiked(isCurrentUserLikedPost());
    }, []);

    const sendLike = () => {
        void axios
            .post(`${baseURL}${Routes.posts}/${postId as unknown as string}/likes`, "", {
                headers: {
                    Authorization: `Bearer ${authKey as string}`,
                },
            })
            .then((response) => {
                setIsLiked(!isLiked);
                setLikesCount(response.data.body.likes.length);
                setMessageType({ message: "", type: "none" });
            })
            .catch(() => setMessageType({ message: "Unexpected error", type: "negative" }));
    };

    return (
        <>
            {!user ? (
                <Popup
                    content="Please, log in to like 😉"
                    on="click"
                    pinned
                    trigger={
                        <img src="/icons/regular-like.svg" className="like" alt="" width="40px" />
                    }
                />
            ) : (
                <>
                    <img
                        onClick={() => sendLike()}
                        src={isLiked ? "/icons/like.svg" : "/icons/regular-like.svg"}
                        className={styles.like}
                        id="like"
                        alt=""
                        width="40px"
                    />
                    <div className={styles.likesCount}> {likesCount} likes</div>
                </>
            )}
        </>
    );
};

export default Like;

import React, { useContext } from "react";
import styles from "./css/userPage.module.css";
import { Item } from "semantic-ui-react";
import { IUserObject } from "../../types/types";
import { NavLink } from "react-router-dom";
import UserContext from "../context/UserContext";
import { IPostObject } from "./../../types/types";

interface IUserInfo {
    pageOwner: IUserObject | undefined;
    userPosts: IPostObject[];
}

const UserInfo: React.FC<IUserInfo> = ({ pageOwner, userPosts }) => {
    const { user } = useContext(UserContext);
    const currentUserId = user?.id;
    const pageOwnerId = pageOwner?.id;

    return (
        <>
            <Item.Group style={{ margin: "0 50%" }}>
                <Item className={styles.itemsContainer} style={{ width: "500px" }}>
                    <img
                        className={styles.avatar}
                        src={pageOwner?.imgUrl || "./default-avatar.png"}
                    />
                    <Item.Content className={styles.content}>
                        <div className={styles.nickNameContainer}>
                            <div className={styles.nickName}> {pageOwner?.nickname}</div>

                            {currentUserId === pageOwnerId && (
                                <NavLink exact to={"/settings/profile-settings"}>
                                    <img src="./icons/settings.svg" alt="" width="25px" />
                                </NavLink>
                            )}
                        </div>
                        <p> {pageOwner?.active ? "🟢 online" : "🟠 offline"}</p>
                        <div className={styles.userName}>{pageOwner?.username}</div>
                        <div className={styles.description}>{pageOwner?.description}</div>
                        <p>Under development</p>
                        <div className={styles.followersContainer}>
                            <div className={styles.followersCountElement}>
                                <div className={styles.value}>{userPosts.length}</div>
                                <div className={styles.valueName}>posts</div>
                            </div>
                            <div className={styles.followersCountElement}>
                                <div className={styles.value}>10</div>
                                <div className={styles.valueName}>followers</div>
                            </div>
                            <div className={styles.followersCountElement}>
                                <div className={styles.value}>0</div>
                                <div className={styles.valueName}>follows</div>
                            </div>
                        </div>
                    </Item.Content>
                </Item>
            </Item.Group>
        </>
    );
};

export default UserInfo;

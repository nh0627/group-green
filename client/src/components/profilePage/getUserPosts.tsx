import axios from "axios";
import { baseURL, Routes } from "./../../constants";
import { IPostObject } from "./../../types/types";

export const getUserPosts = (
    authKey: string | null,
    username: string,
    setUserPosts: React.Dispatch<React.SetStateAction<IPostObject[]>>,
    setMessageType: React.Dispatch<
        React.SetStateAction<{
            message: string;
            type: string;
        }>
    >,
): "" | Promise<void> | null =>
    authKey &&
    axios
        .get(`${baseURL}${Routes.posts}?filterby=username&value=${username}`, {
            headers: {
                Authorization: `Bearer ${authKey}`,
            },
        })
        .then((response) => {
            setUserPosts(response.data.body);
            setMessageType({ message: "", type: "none" });
        })
        .catch(() => setMessageType({ message: "Something went wrong", type: "negative" }));

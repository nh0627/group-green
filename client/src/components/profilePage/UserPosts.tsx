import React from "react";
import styles from "./css/mansory.module.css";
import { ShowNotification } from "../notifications/Notification";
import { NavLink } from "react-router-dom";
import { IPostObject } from "../../types/types";
import { Routes } from "./../../constants";
import Like from "../likes/Like";
interface IUserPosts {
    userPosts: IPostObject[];
}

const UserPosts: React.FC<IUserPosts> = ({ userPosts }) => {
    const { setMessageType, Notification } = ShowNotification();

    const pictureItems = () =>
        userPosts?.map((post: IPostObject, id) => (
            <div className={styles.brick} key={id}>
                <NavLink key={id} exact to={`${Routes.posts}/${post.id}`}>
                    <img className={styles.brickImg} src={`${post.url}`} />
                </NavLink>
                <div className={styles.likesContainer}>
                    <Like post={post} setMessageType={setMessageType} />
                </div>
            </div>
        ));

    const wrappedPictures = (
        <div className={styles.wrapper}>
            <div className={styles.intro}></div>
            <div className={styles.postsContainer}>
                <p className={styles.postsOption}>Posts</p>
                <p className={styles.postsOption}>Liked posts</p>
            </div>
            <Notification />
            <div className={styles.mansory}>{pictureItems()}</div>
        </div>
    );

    return <> {userPosts.length === 0 ? <Notification /> : wrappedPictures}</>;
};

export default UserPosts;

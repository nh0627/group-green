import React, { useEffect, useState } from "react";
import UserInfo from "./UserInfo";
import { StyledContainer } from "../baseModal/StyledContainer";
import { useParams } from "react-router-dom";
import UserPosts from "./UserPosts";
import axios from "axios";
import { baseURL, Routes } from "./../../constants";
import { IPostObject } from "./../../types/types";
import { getUserPosts } from "./getUserPosts";
import { ShowNotification } from "../notifications/Notification";

interface IParamsType {
    username: string;
}

const UserProfilePage: React.FC = () => {
    const { username } = useParams<IParamsType>();
    const [currentUser, setCurrentUser] = useState();
    const [userPosts, setUserPosts] = useState<IPostObject[]>([]);
    const [notFoundError, setNotFoundError] = useState("");
    const [authKey] = useState(localStorage.getItem("token"));
    const { setMessageType, Notification } = ShowNotification();

    useEffect(
        () => (
            void axios
                .get(`${baseURL + Routes.users}/${username}`)
                .then((response) => setCurrentUser(response.data.body))
                .catch(() => setNotFoundError("User not found 😱")),
            void getUserPosts(authKey, username, setUserPosts, setMessageType)
        ),

        [],
    );

    return (
        <StyledContainer>
            {!currentUser ? (
                <div>{notFoundError}</div>
            ) : (
                <>
                    <UserInfo pageOwner={currentUser} userPosts={userPosts} />
                    <UserPosts userPosts={userPosts} />
                </>
            )}
            <Notification />
        </StyledContainer>
    );
};

export default UserProfilePage;

import React from "react";
import { Container, Header, Segment } from "semantic-ui-react";
import styles from "./../profilePage/css/userPage.module.css";
import { NavLink } from "react-router-dom";

const ConfirmationSuccess: React.FC = () => (
    <Container style={{ margin: "100px" }}>
        <Segment
            raised
            placeholder
            style={{
                backgroundImage: "url(./icons/leaves.svg)",
                backgroundRepeat: "no-repeat",
                backgroundSize: "370px",
            }}
        >
            <Header icon>
                <img src="/icons/confetti.svg" style={{ width: "50px" }} />
                Your email is verified!
            </Header>
            <NavLink
                exact
                to={"/auth/signin"}
                className={styles.baseFont}
                style={{
                    textAlign: "center",
                    marginLeft: "50px",
                    fontWeight: "bold",
                }}
            >
                Back to the login page
            </NavLink>
        </Segment>
    </Container>
);

export default ConfirmationSuccess;

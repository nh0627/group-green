import React from "react";
import { Button, Container, Header, Segment } from "semantic-ui-react";
import styles from "./../profilePage/css/userPage.module.css";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { ShowError } from "../notifications/Errors";
import { baseURL, Routes } from "./../../constants";

const ConfirmationPage: React.FC = () => {
    const { switchResponses, ErrorMessage } = ShowError();
    const history = useHistory();
    const key = new URLSearchParams(window.location.search).toString().slice(1);

    const verifyEmail = () => {
        void axios
            .put(`${baseURL}${Routes.users}/confirm/`, { key })
            .then(() => history.replace("/confirmation/confirmation-success"))
            .catch((error) => switchResponses(error.response.status));
    };

    return (
        <Container style={{ margin: "100px" }}>
            <Segment raised placeholder>
                <Header icon>
                    <img src="/icons/leaves.svg" />
                    Verify your email adress
                </Header>
                <p
                    className={styles.baseFont}
                    style={{ textAlign: "center", marginBottom: "30px" }}
                >
                    In order to start using your account, you need to confirm your email adress
                </p>
                <Button type="button" onClick={verifyEmail} color="green">
                    Verify email adress
                </Button>
                <ErrorMessage />
            </Segment>
        </Container>
    );
};

export default ConfirmationPage;

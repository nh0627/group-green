import UserComment from "./UserComment";
import React, { useState, useEffect } from "react";

interface IUserComments {
    comments: Array<{
        avatar: string;
        name: string;
        comment: string;
    }>;
    commentToShow: JSX.Element[];
    ShowMoreBtn: () => void;
}

const UserComments = (): IUserComments => {
    const comments = [
        {
            avatar: "https://images.theconversation.com/files/350865/original/file-20200803-24-50u91u.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=1200.0&fit=crop",
            name: "Hiller",
            comment:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        },

        {
            avatar: "https://bloximages.chicago2.vip.townnews.com/siouxcityjournal.com/content/tncms/assets/v3/editorial/5/65/565fed01-e982-578d-85da-0680b6bc468c/59c434267b53a.image.jpg?crop=440%2C330%2C110%2C0&resize=1200%2C900&order=crop%2Cresize",
            name: "Cat",
            comment:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        },
    ];

    const mapComments = comments.map((item, id) => (
        <UserComment key={id} avatar={item.avatar} name={item.name} comment={item.comment} />
    ));

    const amountCommentsToShow = 2;
    const startAmountOfComments = 3;
    const [commentAmount, setCommentAmount] = useState(startAmountOfComments);
    const [commentToShow, setCommentToShow] = useState(mapComments.slice(0, commentAmount));

    const ShowMoreBtn = () => {
        setCommentAmount(commentAmount + amountCommentsToShow);
    };

    useEffect(() => {
        setCommentToShow(mapComments.slice(0, commentAmount));
    }, [commentAmount]);

    return { comments, commentToShow, ShowMoreBtn };
};

export default UserComments;

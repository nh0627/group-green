import { Comment } from "semantic-ui-react";
import React from "react";

interface IUserComment {
    avatar: string;
    name: string;
    comment: string;
}

const UserComment = ({ avatar, name, comment }: IUserComment): JSX.Element => (
    <Comment.Group>
        <Comment>
            <Comment.Avatar src={avatar} />
            <Comment.Content>
                <Comment.Author>{name}</Comment.Author>
                <Comment.Text>{comment}</Comment.Text>
            </Comment.Content>
        </Comment>
    </Comment.Group>
);

export default UserComment;

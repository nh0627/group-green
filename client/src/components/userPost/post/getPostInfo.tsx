import { useEffect, useState } from "react";
import { baseURL, Routes } from "../../../constants";
import axios from "axios";
import { IPostObject } from "../../../types/types";

const getUserPosts = (
    id: string,
    setnoFoundError: React.Dispatch<React.SetStateAction<string>>,
): IPostObject | undefined => {
    const [userPost, setUserPost] = useState<IPostObject>();

    const getPostInfo = () =>
        axios
            .get(`${baseURL}${Routes.posts}/${id}`)
            .then((response) => {
                setUserPost(response.data.body);
            })
            .catch(() => setnoFoundError("not found 😱"));

    useEffect(() => {
        void getPostInfo();
    }, []);

    return userPost;
};

export default getUserPosts;

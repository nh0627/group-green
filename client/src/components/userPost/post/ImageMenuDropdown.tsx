import React, { useState } from "react";
import { DropdownProps } from "semantic-ui-react";
import { IImageMenuDropdown } from "../../../types/types";
import { useHistory } from "react-router";
import { useParams } from "react-router-dom";

interface IParamsType {
    id: string;
}

const ImageMenuDropDown = (): IImageMenuDropdown => {
    const [option, setOption] = useState("");
    const [open, setOpen] = useState(false);
    const history = useHistory();
    const { id } = useParams<IParamsType>();
    const trigger = <img className="dots-icon" src="/icons/dots.svg" alt="" />;

    const options = [
        { key: "share", text: "Share", value: "share" },
        {
            key: "edit",
            text: "Edit",
            value: "edit",
            onClick: () => {
                history.push(`./edit/${id}`);
            },
        },

        {
            key: "delete",
            text: "Delete",
            value: "delete",
            onClick: () => {
                setOpen(true);
            },
        },
    ];

    const handleSelect = (event: React.SyntheticEvent<HTMLElement, Event>, data: DropdownProps) => {
        setOption(data?.value?.toString() || "");
    };

    return { option, options, trigger, handleSelect, open, setOpen };
};

export default ImageMenuDropDown;

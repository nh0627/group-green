import React, { useEffect, useState } from "react";
import { Button, Form } from "semantic-ui-react";
import { StyledContainer } from "../../baseModal/StyledContainer";
import { showHashtags, separateHashtags, removeHashtagSymbol } from "../../createPost/Hashtag";
import { StyledDownloadError, StyledNewUserPost } from "./StyledNewUserPost";
import Geolocation from "../../createPost/Geolocation";
import { ShowNotification } from "../../notifications/Notification";
import loadGeolocation from "../../../scripts/loadGeolocation";
import { useFormik } from "formik";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router";
import { baseURL, Routes } from "../../../constants";
import { IPostHashtag } from "../../../types/types";
import getUserPosts from "./../post/getPostInfo";
import axios from "axios";

interface IParamsType {
    id: string;
}

const UpdateUserPosts: React.FC = () => {
    const [loaded, setLoaded] = useState(false);
    const { setMessageType, Notification } = ShowNotification();
    const [location, setLocation] = useState("");
    const [authKey] = useState(localStorage.getItem("token"));
    const { id } = useParams<IParamsType>();
    const history = useHistory();
    const [noFoundError, setnoFoundError] = useState("");
    const userPost = getUserPosts(id, setnoFoundError);

    const hashtagsToString = userPost?.hashtags
        ?.map((hashtag: IPostHashtag) => `#${hashtag.name}`)
        .toString();

    const sendUpdatedPost = () =>
        authKey
            ? axios
                  .put(`${baseURL}${Routes.posts}/${id}`, updatedPost, {
                      headers: {
                          Authorization: `Bearer ${authKey}`,
                      },
                  })
                  .then(
                      (response) =>
                          response.status === 200 && history.push(`${Routes.posts}/${id}`),
                  )
                  .catch(() => {
                      setMessageType({
                          message: "Something went wrong",
                          type: "negative",
                      });
                  })
            : setMessageType({
                  message: "You are not logged in",
                  type: "negative",
              });

    const updatePostformik = useFormik({
        initialValues: {
            hashtags: hashtagsToString || "",
            description: userPost?.description,
        },
        onSubmit: async () => {
            void sendUpdatedPost();
        },
        enableReinitialize: true,
    });

    const updatedPost = {
        hashtags: removeHashtagSymbol(separateHashtags(updatePostformik.values.hashtags || "")),
        description: updatePostformik?.values?.description,
        location: location ? location : userPost?.location,
    };

    useEffect(() => {
        loadGeolocation(() => {
            setLoaded(true);
        });
    }, []);

    return (
        <StyledContainer>
            <StyledNewUserPost>
                <div className="left-side">
                    <img className="post-image" src={userPost?.url} alt="" />
                    <Notification />
                    <div>{noFoundError}</div>
                </div>

                <div className="right-side">
                    <Form type="submit" onSubmit={() => sendUpdatedPost()}>
                        <Form.Input
                            onChange={updatePostformik.handleChange}
                            id="hashtags"
                            label="Hashtags"
                            value={updatePostformik?.values?.hashtags}
                        />
                        <div className="hashtags-container">
                            {showHashtags(separateHashtags(updatePostformik.values.hashtags || ""))}
                        </div>

                        <Form.TextArea
                            onChange={updatePostformik.handleChange}
                            id="description"
                            label="Description"
                            value={updatePostformik?.values?.description}
                        />

                        {loaded ? (
                            <Geolocation setLocation={setLocation} />
                        ) : (
                            <StyledDownloadError>
                                cannot download geolocation library
                            </StyledDownloadError>
                        )}
                        <div>Post location: {location ? location : userPost?.location}</div>

                        <div
                            style={{
                                display: "flex",
                                justifyContent: "space-between",
                                marginTop: "100px",
                            }}
                        >
                            <Button
                                type="submit"
                                onClick={() => history.replace(`${Routes.posts}/${id}`)}
                            >
                                Cancel
                            </Button>
                            <Button color="green" type="submit">
                                Save
                            </Button>
                        </div>
                    </Form>
                </div>
            </StyledNewUserPost>
        </StyledContainer>
    );
};

export default UpdateUserPosts;

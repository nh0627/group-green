import React, { useContext, useState } from "react";
import { Picker } from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";
import ReadMore from "./ReadMore";
import {
    SendCommentButton,
    SendCommentInput,
    StyledFollowButton,
    StyledLoadMoreBtn,
} from "./StyledNewUserPost";
import { IPostHashtag } from "../../../types/types";
import { showHashtags } from "../../createPost/Hashtag";
import UserComments from "../postComments/UserComments";
import { IPostObject } from "../../../types/types";
import { formatDistance } from "date-fns";
import { NavLink } from "react-router-dom";
import UserContext from "../../context/UserContext";
interface IImageOwner {
    userPost: IPostObject;
}

// TODO: Replace initial likes and followers with actual implementation
const ImageOwner: React.FC<IImageOwner> = ({ userPost }) => {
    const [emojiPickerState, SetEmojiPicker] = useState(false);
    const { comments, commentToShow, ShowMoreBtn } = UserComments();
    const mappedHashtags = userPost?.hashtags?.map((hashtag: IPostHashtag) => `#${hashtag.name}`);
    const { user } = useContext(UserContext);
    const postOwnerId = userPost?.user.id;
    const userId = user?.id;

    function triggerPicker(event: React.MouseEvent<HTMLElement>) {
        event.preventDefault();
        SetEmojiPicker(!emojiPickerState);
    }

    return (
        <div className="right-side">
            <div className="user-header">
                <NavLink exact to={`/${userPost?.user?.username}`}>
                    <img
                        className="avatar-image"
                        src={userPost?.user?.imgUrl || "/default-avatar.png"}
                        alt=""
                    />
                </NavLink>
                <div className="userName-container">
                    <p className="user-name">{userPost?.user?.username}</p>
                    <p className="followers">followers 100</p>
                </div>

                {postOwnerId !== userId ? (
                    <StyledFollowButton color="blue">Follow</StyledFollowButton>
                ) : (
                    <div></div>
                )}
            </div>
            <div className="hashtags-container">{showHashtags(mappedHashtags)}</div>
            <div className="description">
                <ReadMore>{userPost?.description || ""}</ReadMore>
            </div>
            <div className="time">
                {formatDistance(new Date(userPost?.createdAt), new Date(), {
                    addSuffix: true,
                })}
            </div>
            <div className="location-container">
                <img src="/icons/location.svg" className="icon" alt="" />
                <div className="location">{userPost?.location}</div>
            </div>
            <div className="icons-container">
                <img src="/icons/chat.svg" className="icon" alt="" />
            </div>

            <div className="comments-container">
                <p style={{ fontWeight: 700 }}>This is a placeholder</p>
                {commentToShow.map((el) => el)}
                {commentToShow.length < comments.length && (
                    <StyledLoadMoreBtn onClick={ShowMoreBtn}>Show more</StyledLoadMoreBtn>
                )}
            </div>

            <div className="post-container" style={{ position: "relative" }}>
                <img onClick={triggerPicker} src="/icons/smile.svg" className="open-emoji" />
                <SendCommentInput placeholder="leave a comment..." />
                <SendCommentButton>Post</SendCommentButton>
                <div className="picker-style" style={{ position: "absolute", bottom: "50px" }}>
                    {emojiPickerState && <Picker emoji="point_up" />}
                </div>
            </div>
        </div>
    );
};

export default ImageOwner;

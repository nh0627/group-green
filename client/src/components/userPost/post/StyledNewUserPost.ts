import styled from "styled-components";
import { Button, Input } from "semantic-ui-react";

export const StyledNewUserPost = styled.div`
    &&& {
        display: flex;
        font-family: "Dosis", sans-serif;
				align-items: initial;
    }

    &&& .left-side {
        display: flex;
        flex-direction: column;
				padding: 0px 15px;
        width: 55%;
    }

    &&& .post-menu-container {
        display: flex;
        justify-content: space-around;
    }

    &&& .likes-container {
        margin-top: 10px;
        display: flex;
        align-items: center;
        font-size: 20px;
        font-weight: 600;
				
    }

    &&& .like {
        margin-right: 20px;
    }

    &&& .right-side {
        width: 40%;
        padding: 0px 15px;
    }

    &&& .post-image {
        width: 500px;
        margin: 0px auto;
				border-radius: 10px;

    }

    &&& .dots-icon {
        margin-top: 10px;
        width: 40px;
    }

    &&& .avatar-image {
        width: 80px;
				border-radius: 420px;
    }

    &&& .user-header {
        display: flex;
        justify-content: space-between;
    }

    &&& .userName-container {
        display: flex;
        flex-direction: column;
        align-items: center;
        margin-top: 10px;
    }

    &&& .user-name {
        font-size: 20px;
        font-weigth: 500;
        margin: 5px;
    }

    &&& .followers {
        font-family: "Dosis", sans-serif;
        font-size: 15px;
        font-weight: 500;
    }

    &&& .hashtags-container {
        display: flex;
        flex-wrap: wrap;
        width: 420px;
        margin: 20px 0;
    }

    &&& .description {
        font-size: 16px;
        font-weight: 500;
    }

    &&& .location-container {
        display: flex;
        margin-top: 10px;
        align-items: center;
    }

		&&& .icons-container {
		margin-top: 10px;
		display: flex;
    align-items: center;
		}


    &&& .icons-container,
		display: flex;
    align-items: center;
    .time {
        margin: 10px 0;
    }

    &&& .icon {
        width: 25px;
        margin-right: 5px;
    }

    &&& .comments-container {
        margin: 1.75em 0 0.75em;
        border-bottom: 1px solid rgba(0, 0, 0, 0.05);
        height: 25vh;
        overflow-y: auto;
        display: flex;
        flex-direction: column;
    }

    &&& .post-container {
        display: flex;
        align-items: center;
    }

    &&& .icon-like {
        margin-right: 15px;
    }

    &&& .open-emoji {
        width: 30px;
    }

    &&& .ui.dropdown > .dropdown.icon:before {
        content: "";
    }

    @media (max-width: 991px) {
        &&& .left-side,
        &&& .right-side {
            width: 50%;
        }

        &&& .post-image {
            width: 350px;
        }
    }

    @media (max-width: 689px) {
        &&& .user-header {
            flex-direction: column;
            align-items: center;
        }

        &&& .user-header {
            flex-direction: column;
            align-items: center;
        }

        &&& .hashtags-container {
            width: 290px;
        }

        &&& .post-menu-container {
            justify-content: space-between;
        }

        &&& .likes-container {
            font-size: 15px;
					
        }

        &&& .like {
            width: 25px;
        }

        &&& .dots-icon {
            width: 25px;
        }

        @media (max-width: 600px) {
            &&& {
                display: flex;
                flex-direction: column;
                align-items: center;
            }

            &&& .left-side,
            &&& .right-side {
                width: 100%;
            }

            &&& .user-header {
                flex-direction: inherit;
            }
        }
    }
`;

export const StyledFollowButton = styled(Button)`
    &&& {
        margin: 20px;
    }
`;

export const SendCommentButton = styled.button`
    &&& {
        color: rgba(3, 169, 244, 0.61);

        background: none;
        border: none;
        font-weight: 600;
    }
`;

export const SendCommentInput = styled(Input)`
    &&& input {
        width: 350px;
        border: 1px solid white;
    }

    @media (max-width: 991px) {
        &&& {
            width: 300px;
        }
    }

    @media (max-width: 689px) {
        &&& {
            width: 180px;
        }
    }
`;

export const StyledLoadMore = styled.button`
    &&& {
        border: none;
        background: none;
        color: #4c5fc6;
        font-weight: 600;
        padding: 5px 0;
        font-size: 14px;
        display: flex;
    }
`;

export const StyledLoadMoreBtn = styled.button`
    &&& {
        border: none;
        background: none;
        color: #64666c;
        padding: 5px 0;
        font-size: 13px;
        display: flex;
        justify-content: center;
    }
`;

export const StyledDownloadError = styled.div`
    &&& {
        color: red;
        textalign: center;
        fontsize: 10px;
    }
`;

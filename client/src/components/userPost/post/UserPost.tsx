import React, { useState } from "react";
import { StyledNewUserPost } from "./StyledNewUserPost";
import { StyledContainer } from "./../../baseModal/StyledContainer";
import { useParams } from "react-router";
import PostImage from "./PostImage";
import ImageOwner from "./ImageOwner";
import getUserPosts from "./getPostInfo";

interface IParamsType {
    id: string;
}

const NewUserPost: React.FC = () => {
    const { id } = useParams<IParamsType>();
    const [isDeleted, setDeleted] = useState(false);
    const [noFoundError, setnoFoundError] = useState("");
    const userPost = getUserPosts(id, setnoFoundError);

    return (
        <StyledContainer>
            {isDeleted ? (
                <div> This post was deleted ¯\_(ツ)_/¯</div>
            ) : !userPost ? (
                <div>{noFoundError}</div>
            ) : (
                <StyledNewUserPost>
                    <PostImage id={id} setDeleted={setDeleted} userPost={userPost} />
                    <ImageOwner userPost={userPost} />
                </StyledNewUserPost>
            )}
        </StyledContainer>
    );
};
export default NewUserPost;

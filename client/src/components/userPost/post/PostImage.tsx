import React, { useContext, useState } from "react";
import { Confirm, Dropdown } from "semantic-ui-react";
import { ShowNotification } from "../../notifications/Notification";
import { IPostObject } from "../../../types/types";
import UserContext from "../../context/UserContext";
import Like from "../../likes/Like";
import axios from "axios";
import { baseURL, Routes } from "../../../constants";
import ImageMenuDropDown from "./ImageMenuDropdown";
interface IPostImage {
    setDeleted: React.Dispatch<React.SetStateAction<boolean>>;
    id: string;
    userPost: IPostObject | undefined;
}

const PostImage: React.FC<IPostImage> = ({ setDeleted, id, userPost }) => {
    const [authKey] = useState(localStorage.getItem("token"));
    const { setMessageType, Notification } = ShowNotification();
    const { option, trigger, options, handleSelect, open, setOpen } = ImageMenuDropDown();
    const { user } = useContext(UserContext);
    const postOwnerId = userPost?.user.id;
    const userId = user?.id;

    const deleteUserPost = () =>
        authKey !== null
            ? axios
                  .delete(`${baseURL}${Routes.posts}/${id}`, {
                      headers: {
                          Authorization: `Bearer ${authKey}`,
                      },
                  })
                  .then((response) => response.status === 200 && (setOpen(false), setDeleted(true)))
                  .catch(() => {
                      setOpen(false);
                      setMessageType({ message: "Something went wrong", type: "negative" });
                  })
            : (setMessageType({ message: "You are not logged in", type: "negative" }),
              setOpen(false));

    return (
        <div className="left-side">
            <img className="post-image" src={userPost?.url} alt="" />
            <div className="post-menu-container">
                <div className="likes-container">
                    <Like post={userPost} setMessageType={setMessageType} />
                </div>

                {postOwnerId === userId && (
                    <Dropdown
                        value={option}
                        trigger={trigger}
                        options={options}
                        onChange={(event, data) => handleSelect(event, data)}
                    />
                )}
            </div>
            <Confirm
                open={open}
                header="Delete"
                content="Are you sure you want to delete this post?"
                onCancel={() => {
                    setOpen(false);
                }}
                onConfirm={() => deleteUserPost()}
            />
            <Notification />
        </div>
    );
};

export default PostImage;

import React, { useState } from "react";
import { StyledLoadMore } from "./StyledNewUserPost";
interface IReadMore {
    children: string;
}

const ReadMore = ({ children }: IReadMore): JSX.Element => {
    const text = children;
    const [isReadMore, setIsReadMore] = useState(true);
    const maxTextLength = 150;

    return (
        <div>
            {isReadMore ? text.slice(0, maxTextLength) : text}
            {text.length > maxTextLength && (
                <StyledLoadMore
                    onClick={() => setIsReadMore(!isReadMore)}
                    className="load-more-desc"
                >
                    {isReadMore ? "...read more" : " show less"}
                </StyledLoadMore>
            )}
        </div>
    );
};

export default ReadMore;

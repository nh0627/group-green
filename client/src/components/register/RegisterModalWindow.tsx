import React from "react";
import { Form } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import { SignupSchema } from "../formikSchemas/FormikSchemas";
import { FormikProps, useFormik } from "formik";
import { ShowPassword } from "../signIn/showPassword";
import { ShowError, FieldError } from "../notifications/Errors";
import { ModalBody } from "../baseModal/BaseModal";
import axois from "axios";
import { IOpen, IUser } from "./../../types/types";
import { baseURL, Routes } from "./../../constants";
import { ShowNotification } from "../notifications/Notification";
import { useHistory } from "react-router-dom";

export interface IRegisterModaProps {
    email: string;
    password: string;
    username: string;
    passwordConfirmation: string;
}

const SignupModalWindow: React.FC<IOpen> = ({ isOpen }) => {
    const { switchResponses, setError, ErrorMessage } = ShowError();
    const { passwordShown, togglePasswordVisiblity, eye } = ShowPassword();
    const { setMessageType, Notification } = ShowNotification();
    const history = useHistory();

    const signUpformik: FormikProps<IRegisterModaProps> = useFormik({
        initialValues: {
            email: "",
            password: "",
            username: "",
            passwordConfirmation: "",
        },
        validationSchema: SignupSchema as unknown,
        onSubmit: () => {
            void registerUser(newUser);
        },
    });

    const newUser: IUser = {
        email: signUpformik.values.email,
        password: signUpformik.values.password,
        username: signUpformik.values.username,
    };

    const registerUser = (newUser: IUser) => {
        const timeBeforeRedirect = 5000;
        axois
            .post(baseURL + Routes.users, newUser)
            .then(() => {
                setError("");
                setMessageType({
                    message: "Check your email to complete registration",
                    type: "positive",
                });
                setTimeout(() => history.push("/auth/signin"), timeBeforeRedirect);
            })
            .catch((error) =>
                error.response
                    ? switchResponses(error.response.status)
                    : switchResponses("no connection"),
            );
    };

    return (
        <ModalBody
            title="Sign up"
            isOpen={isOpen}
            size="tiny"
            formik={signUpformik}
            buttonName="Sign up"
            onSubmit={signUpformik.handleSubmit}
        >
            {signUpformik.errors.username ? (
                <FieldError text={signUpformik.errors.username} />
            ) : null}
            <Form.Input
                onChange={signUpformik.handleChange}
                required
                id="username"
                value={signUpformik.values.username}
                icon="user"
                iconPosition="left"
                label="Username"
                placeholder="Username"
            />
            {signUpformik.errors.email ? <FieldError text={signUpformik.errors.email} /> : null}
            <Form.Input
                onChange={signUpformik.handleChange}
                required
                id="email"
                value={signUpformik.values.email}
                icon="envelope outline"
                iconPosition="left"
                label="Email"
                placeholder="Email"
            />
            {signUpformik.errors.password ? (
                <FieldError text={signUpformik.errors.password} />
            ) : null}
            <div style={{ position: "relative" }}>
                <Form.Input
                    onChange={signUpformik.handleChange}
                    required
                    id="password"
                    value={signUpformik.values.password}
                    icon="lock"
                    iconPosition="left"
                    label="Password"
                    type={passwordShown ? "text" : "password"}
                    placeholder="Password"
                />
                <i
                    style={{ position: "absolute", right: "10px", bottom: "10px" }}
                    onClick={togglePasswordVisiblity}
                >
                    {eye}
                </i>
            </div>

            {signUpformik.errors.passwordConfirmation ? (
                <FieldError text={signUpformik.errors.passwordConfirmation} />
            ) : null}

            <Form.Input
                onChange={signUpformik.handleChange}
                required
                id="passwordConfirmation"
                iconPosition="left"
                label="Confirm password"
                value={signUpformik.values.passwordConfirmation}
                type={passwordShown ? "text" : "password"}
            />

            <ErrorMessage />
            <Notification />
        </ModalBody>
    );
};

export default SignupModalWindow;

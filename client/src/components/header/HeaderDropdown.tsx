import { useState } from "react";
import React, { useContext } from "react";
import UserContext from "../context/UserContext";
import { IUserContext } from "../../types/types";
import { Dropdown, DropdownProps } from "semantic-ui-react";
import { UserAvatar } from "./StyledHeader";
import { useHistory } from "react-router";
import handleSignOut from "../signIn/signOut";

const HeaderDropdown: React.FC = () => {
    const [option, setOption] = useState("");
    const history = useHistory();
    const { user } = useContext(UserContext) as IUserContext;
    const userName = user.username;
    const logOut = handleSignOut();

    const trigger = (
        <UserAvatar
            className="user-icon"
            src={user?.imgUrl || "/default-avatar.png"}
            alt=""
            width="25px"
        />
    );

    const options = [
        {
            key: "user",
            text: (
                <span>
                    Signed in as <strong>{user.username}</strong>{" "}
                </span>
            ),
            disabled: true,
        },
        {
            key: "profile",
            text: "Profile",
            value: "profile",
            icon: "user circle outline",
            onClick: () => {
                location.assign(`/${userName}`);
            },
        },
        {
            key: "settings",
            text: "Settings",
            value: "settings",
            icon: "settings",
            onClick: () => {
                history.push("/settings/profile-settings");
            },
        },
        {
            key: "sign-out",
            text: "Sign Out",
            value: "sign-out",
            icon: "log out",
            onClick: () => {
                logOut();
            },
        },
    ];

    const handleSelect = (event: React.SyntheticEvent<HTMLElement, Event>, data: DropdownProps) => {
        setOption(data?.value?.toString() || "");
    };

    return (
        <Dropdown
            value={option}
            trigger={trigger}
            options={options}
            onChange={(event, data) => handleSelect(event, data)}
        />
    );
};

export default HeaderDropdown;

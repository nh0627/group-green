import React, { useContext } from "react";
import { Menu, Container } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import UserInfoHeader from "./UserInfoHeader";
import { IUserContext } from "../../types/types";
import UserContext from "../context/UserContext";
import UserMenu from "./UserMenu";
import HeaderInput from "./HeaderInput";
import { StyledMenu } from "./StyledHeader";
import GlobalErrorContext from "./../context/GlobalErrorContext";

const Header = (): JSX.Element => {
    const { user } = useContext(UserContext) as IUserContext;
    const { Notification } = useContext(GlobalErrorContext);

    return (
        <>
            <StyledMenu size="small">
                <Container>
                    <HeaderInput />
                    <Menu.Menu position="right">
                        {user ? <UserMenu /> : <UserInfoHeader />}
                    </Menu.Menu>
                </Container>
            </StyledMenu>
            <Container>
                <Notification />
            </Container>
        </>
    );
};

export default Header;

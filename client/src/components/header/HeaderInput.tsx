import React from "react";
import { StyledInput, StyledIcon } from "./StyledHeader";

const HeaderInput: React.FC = () => (
    <>
        <img src="/icons/leaves.svg" alt="" width="30xp" />
        <StyledIcon>GreenGram</StyledIcon>
        <StyledInput icon="search" iconPosition="left" placeholder="Placeholder..." />
    </>
);
export default HeaderInput;

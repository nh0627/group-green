import React from "react";
import HeaderDropdown from "./HeaderDropdown";
import { NavLink } from "react-router-dom";
import { StyledUserIcons } from "./StyledHeader";

const UserMenu: React.FC = () => (
    <>
        <StyledUserIcons>
            <img className="icon-item" src="/icons/compass.svg" alt="" width="24px" />
            <img className="icon-item" src="/icons/chat.svg" alt="" width="25px" />
            <img className="icon-item" src="/icons/ringing.svg" alt="" width="25px" />
            <NavLink exact to={"/create-post"}>
                <img className="icon-item" src="/icons/add.svg" alt="" width="25px" />
            </NavLink>
            <HeaderDropdown />
        </StyledUserIcons>
    </>
);

export default UserMenu;

import styled from "styled-components";
import { Input, Button, Menu } from "semantic-ui-react";

export const StyledInput = styled(Input)`
    &&& {
        width: 400px;
        margin: 5px;
				padding: 0;
    }
		
    @media (max-width: 600px) {
			&&& {
				width: 150px;
			}
`;

export const StyledMenu = styled(Menu)``;

export const StyledIcon = styled(Button)`
    &&& {
        background: none;
        color: green;
        font-size: 23px;
        padding: 0 10px 0 0;
        font-family: "Dosis", sans-serif;
    }
`;

export const StyledHeaderButton = styled(Button)`
    &&& {
        margin-top: 2px;
    }

    &&& p {
        bordertopleftradius: 2.285714rem;
        padding: 0px;
    }

    &&& .ui.dropdown {
        display: flex;
    }

    @media (max-width: 991px) {
        &&& p {
            width: 50px;
        }
    }

    @media (max-width: 600px) {
        &&& {
            padding: 8px;
        }
    }
`;

export const StyledUserIcons = styled.div`
    &&& .ui.dropdown {
        height: 26px;
    }

    &&& .ui.dropdown > .dropdown.icon:before {
        content: "";
    }

    &&& a {
        height: 26px;
    }

    &&& {
        display: flex;
        align-items: center;
    }
    &&& .icon-item {
        margin-left: 20px;
    }

    &&& .user-icon {
        margin-left: 20px;
        border-radius: 15px;
        border: 2px solid gray;
    }
`;

export const UserAvatar = styled.img`
    &&& {
			width: 30px,
			margin-left: 20px,
			
			border-radius: 30px,
            height: 100%
    }
`;

import React from "react";
import { NavLink } from "react-router-dom";
import { Button } from "semantic-ui-react";
import { StyledHeaderButton } from "./StyledHeader";

const UserInfoHeader: React.FC = () => (
    <Button.Group>
        <NavLink exact to={"/auth/signin"}>
            <StyledHeaderButton color="blue">
                <p>Login</p>
            </StyledHeaderButton>
        </NavLink>

        <Button.Or />
        <NavLink exact to={"/auth/sign-up"}>
            <StyledHeaderButton color="green">
                <p>Sign up</p>
            </StyledHeaderButton>
        </NavLink>
    </Button.Group>
);
export default UserInfoHeader;

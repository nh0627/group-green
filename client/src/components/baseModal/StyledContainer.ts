import { Container } from "semantic-ui-react";
import styled from "styled-components";

export const StyledContainer = styled(Container)`
    &&& {
        margin-top: 50px;
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }
`;

export const ModalTitle = styled.p`
    &&& {
        font-size: 25px;
        font-weight: 500;
        color: gray;
        font-family: "Dosis", sans-serif;
    }
`;

export const ModalContainer = styled(Container)`
    &&& {
        display: flex;
        justify-content: space-between;
    }
`;

import React, { ReactElement, useState } from "react";
import { Modal, Grid, Form } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import { IModalProps } from "../../types/types";
import { useHistory } from "react-router-dom";
import { ModalContainer, ModalTitle } from "./StyledContainer";

export const ModalBody = ({
    title,
    isOpen,
    size,
    formik,
    buttonName,
    onSubmit,
    children,
}: IModalProps): ReactElement => {
    const [open, setOpen] = useState(isOpen);
    const history = useHistory();

    return (
        <Modal
            size={size}
            onClose={() => {
                setTimeout(() => history.push("/"), 0);
                setOpen(false);
            }}
            onOpen={() => setOpen(true)}
            open={open}
        >
            <Grid centered columns={1}>
                <Grid.Column width="12" style={{ padding: "60px 40px" }}>
                    <ModalTitle>{title}</ModalTitle>
                    <Form onSubmit={onSubmit} type="submit">
                        {children}
                        <ModalContainer>
                            <Form.Button
                                type="button"
                                onClick={() => {
                                    formik.resetForm();
                                    setTimeout(() => history.push("/"), 0);
                                    setOpen(false);
                                }}
                            >
                                Cancel
                            </Form.Button>
                            <Form.Button type="submit" color="green">
                                {buttonName}
                            </Form.Button>
                        </ModalContainer>
                    </Form>
                </Grid.Column>
            </Grid>
        </Modal>
    );
};

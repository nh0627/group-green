import React from "react";
import { Redirect, Route } from "react-router-dom";
import ConfirmationPage from "./components/confirmation/ConfirmationPage";
import ConfirmationSuccess from "./components/confirmation/ConfirmationSuccess";
import UserContext from "./components/context/UserContext";
import Header from "./components/header/Header";
import UserProfilePage from "./components/profilePage/UserProfilePage";
import { saveToken } from "./components/signIn/saveToken";
import { useUser } from "./components/signIn/UseUser";
import SignInModalWindow from "./components/signIn/SignInModalWindow";
import SignupModalWindow from "./components/register/RegisterModalWindow";
import CreatePost from "./components/createPost/CreatePost";
import NewUserPost from "./components/userPost/post/UserPost";
import UpdateUserPosts from "./components/userPost/post/UpdateUserPost";
import SettingsPage from "./components/settingsPage/SettingsPage";
import ChangePassword from "./components/settingsPage/ChangePassword";
import GlobalErrorContext from "./components/context/GlobalErrorContext";
import { ShowNotification } from "./components/notifications/Notification";

const Routes: React.FC = () => {
    const { saveUser, user } = useUser();
    const { setMessageType, Notification } = ShowNotification();
    const username = user?.username;

    return (
        <>
            <GlobalErrorContext.Provider value={{ setMessageType, Notification }}>
                <UserContext.Provider value={{ user }}>
                    <Route exact path="/confirmation/confirmation-success">
                        <ConfirmationSuccess />
                    </Route>
                    <Route exact path="/confirm/:key">
                        <ConfirmationPage />
                    </Route>

                    <Route exact path="/auth/signin">
                        <Header />
                        <SignInModalWindow
                            saveToken={saveToken}
                            saveUser={saveUser}
                            isOpen={true}
                        />
                    </Route>

                    <Route exact path="/auth/sign-up">
                        <Header />
                        <SignupModalWindow isOpen={true} />
                    </Route>

                    <Route exact path="/create-post">
                        <CreatePost isOpen={true} />
                    </Route>

                    <Route exact path="/">
                        <Header />
                        {user && <Redirect to={`/${username || ""}`} />}
                    </Route>

                    <Route exact path="/:username">
                        <Header />
                        <UserProfilePage />
                    </Route>

                    <Route exact path="/posts/:id">
                        <Header />
                        <NewUserPost />
                    </Route>

                    <Route exact path="/posts/edit/:id">
                        <Header />
                        <UpdateUserPosts />
                    </Route>

                    <Route exact path="/settings/profile-settings">
                        <Header />
                        <SettingsPage />
                    </Route>

                    <Route exact path="/settings/password-settings">
                        <Header />
                        <ChangePassword />
                    </Route>
                </UserContext.Provider>
            </GlobalErrorContext.Provider>
        </>
    );
};

export default Routes;

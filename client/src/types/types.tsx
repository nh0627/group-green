import { FormikProps, FormikValues } from "formik";
import { ISignInModaProps } from "./../components/signIn/SignInModalWindow";
import { IRegisterModaProps } from "../components/register/RegisterModalWindow";
import { ICreatePostProps } from "../components/createPost/CreatePost";
import { DropdownProps } from "semantic-ui-react";

export interface IUser {
    email: string | undefined;
    password: string | undefined;
    username?: string | undefined;
}

export interface IUsers {
    allUsers: IUserObject[] | undefined;
}

export interface IUserObject {
    active: boolean;
    confirmed: boolean;
    createdAt: string;
    description: string | null;
    email: string;
    id: number;
    imgUrl: string | undefined;
    modifiedAt: string;
    nickname: string | null;
    username: string;
}

export interface IButtonTitle {
    buttonName: string;
    formik:
        | FormikProps<FormikValues>
        | FormikProps<ISignInModaProps>
        | FormikProps<IRegisterModaProps>;
}

export interface IModalProps {
    title: string;
    isOpen: boolean;
    size: "mini" | "tiny" | "small" | "large" | "fullscreen" | undefined;
    formik:
        | FormikProps<FormikValues>
        | FormikProps<ISignInModaProps>
        | FormikProps<IRegisterModaProps>
        | FormikProps<ICreatePostProps>;
    buttonName: string;
    onSubmit: (e?: React.FormEvent<HTMLFormElement>) => void;
    children: React.ReactNode;
}

export interface IShowPassword {
    passwordShown: boolean;
    togglePasswordVisiblity: () => void;
    eye: JSX.Element;
}

export interface IErrors {
    switchResponses: (response: number | string) => void;
    setError: React.Dispatch<React.SetStateAction<string>>;
    ErrorMessage: () => JSX.Element | null;
}

export interface IToken {
    token: string;
}

export interface IUserContext {
    user: IUserObject;
}

export interface IOpen {
    isOpen: boolean;
}

export interface IShowNotification {
    message: string;
    setMessage: React.Dispatch<React.SetStateAction<string>>;
    Notification: (text: { text: string }) => JSX.Element | null;
}

export interface INotification {
    setMessageType: React.Dispatch<
        React.SetStateAction<{
            message: string;
            type: string;
        }>
    >;
    Notification: () => JSX.Element | null;
}

export interface IHeaderProps {
    saveToken: (userToken: IToken) => void;
    saveUser: (userInfo: IUserObject) => void;
}

export interface ITokenProps {
    saveToken: (userToken: string) => void;
    saveUser: (userInfo: IUserObject) => void;
    isOpen: boolean;
}

export interface IUseUserReturn {
    user: IUserObject | undefined;
    saveUser: (userInfo: IUserObject) => void;
}

export interface IUseTokenReturn {
    token: string | null;
    saveToken: (userToken: IToken) => void;
}

export interface IImageMenuDropdown {
    option: string;
    options: Array<
        | {
              key: string;
              text: string;
              value: string;
              onClick?: undefined;
          }
        | {
              key: string;
              text: string;
              value: string;
              onClick: () => void;
          }
    >;
    trigger: JSX.Element;
    handleSelect: (event: React.SyntheticEvent<HTMLElement, Event>, data: DropdownProps) => void;
    open: boolean;
    setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export interface IFilePreview {
    ShowImage: () => JSX.Element;
    name: string;
}

export interface INewPost {
    url?: string;
    hashtags: string[];
    description: string;
    location: string;
}

export interface IPostHashtag {
    createdAt: string;
    id: number;
    name: string;
}
export interface IPostImage {
    createdAt: string;
    description: string | "";
    id: number;
    location: string;
    modifiedAt: string;
    url: string;
}

export interface IPostObject {
    createdAt: string;
    description: string | "";
    id: number;
    location: string;
    modifiedAt: string;
    url: string;
    hashtags: IPostHashtag[];
    user: IUserObject;
    likes: IUserObject[];
}

Greengram💚
======================

> Instagram clone project🖼

## Features
### User / Auth management
- Sign up
- Receive a confirmation email after sign up
- Sign in with JWT tokens
- With a refresh token, a user can be logged in for 2 weeks
- Retrieve and update their data
- Signout

### Post management
- Retrieve a post
- Retrieve a post in a list filtered by a hashtag, a username, and a user's id
- Save, update, and delete a post with hashtags and a media file

### Like management
- Like a post
- Show how many likes it has and the list of liked users
- See a user's liked list

## Built with

Greengram is built with a number of open source projects to work properly:

### Base languages
* [TypeScript](https://www.typescriptlang.org/)
* [Node.js](https://nodejs.org/en/)
* [JavaScript](https://developer.mozilla.org)

### Web
* [React](https://reactjs.org/)
* [Semantic UI](https://semantic-ui.com/)

### Server(API)
* [Express](https://expressjs.com/)
* [typeorm](https://typeorm.io)

## Setup
1. Clone this repo and run `docker-compose up --build` to install and start both `api` and `client`.
2. You can access them at `localhost:3000` for `client` and `localhost:5000` for `api`🎈

## API List
| Resource | Endpoint         | Method | Description                |
|----------|------------------|--------|----------------------------|
| User     | /users/:username | GET    | Get a user                 |
| User     | /users           | POST   | Save a user (Signup)       |
| User     | /users           | PUT    | Update a user              |
| Post     | /users/:id/likes | GET    | Get a user's liked posts   |
| User     | /users/confirm   | PUT    | Confirm a user             |
| User     | /auth/signin     | POST   | Signin                     |
| User     | /auth/signout    | POST   | Signout                    |
| User     | /auth/token      | GET    | Get a new access token     |
| Post     | /posts           | GET    | Get a post list (filterby=userId | username | hashtag&value=${value}) |
| Post     | /posts/:id       | GET    | Get one post               |
| Post     | /posts           | POST   | Save a post                |
| Post     | /posts/:id       | PUT    | Update a post              |
| Post     | /posts/:id       | DELETE | Remove a post              |
| Post     | /posts/:id/likes | POST   | Like a post                |
| -        | /upload          | POST   | File uploads               |
| -        | /clientKeys      | GET    | Get keys to operate client |